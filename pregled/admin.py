from django.contrib import admin
from django import forms
from django.utils.translation import gettext_lazy as _

from struktura.models import Ekipa
from .models import Obvestilo

class ObvestiloForm(forms.ModelForm):

    class Meta:
        model = Obvestilo
        exclude = ['cas_objave', 'ekipe']
        widgets = {
            'besedilo': forms.Textarea(),
        }


class ObvestiloAdmin(admin.ModelAdmin):
    form = ObvestiloForm

admin.site.register(Obvestilo, ObvestiloAdmin)
