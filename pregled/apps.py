from django.apps import AppConfig


class PregledConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'pregled'
