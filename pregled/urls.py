from django.urls import path, register_converter
from django.conf import settings

from . import views
import converters

register_converter(converters.makeConverter(settings.HASHIDS_SALT['main']), 'mhash')
register_converter(converters.makeConverter(settings.HASHIDS_SALT['ekipa']), 'ehash')
register_converter(converters.makeConverter(settings.HASHIDS_SALT['info']), 'ihash')
register_converter(converters.makeConverter(settings.HASHIDS_SALT['rez_view']), 'rv_hash')

app_name = 'pregled'
urlpatterns = [
    path('<mhash:key>/', views.index, name = 'index'),
    path('import_export/<mhash:key>/', views.file.import_export, name = 'import_export'),
    path('download/<mhash:key>/', views.file.download, name = 'download'),
    path('upload/<mhash:key>/', views.file.upload, name = 'upload'),
    path('kontrole/<mhash:key>/', views.kontrole, name = 'kontrole'),
    path('proge/<mhash:key>/', views.proge, name = 'proge'),
    path('ekipe/<mhash:key>/', views.ekipe, name = 'ekipe'),
    path('rezultati/<mhash:key>/', views.rezultati, name = 'rezultati'),
    path('rezultati/v/<rv_hash:key>/', views.rezultati_v, name = 'rezultati_v'),

    path('info/<mhash:key>/<int:ekipa_id>/', views.info.ekipa, name = 'info_ekipa'),
    path('info/<mhash:key>/', views.info.index, name = 'info_index'),
    path('info/<ihash:key>/<str:ime>/', views.info.view, name = 'info_view'),
    path('info/<ihash:key>/', views.info.view, name = 'info_view'),

    path('obvestila/<mhash:key>/', views.obvestilo.obvestila, name = 'obvestila'),
    path('obvestilo/<mhash:key>/<int:obvestilo_id>', views.obvestilo.obvestilo,
        name = 'obvestilo'),
    path('obvestilo/<mhash:key>/new', views.obvestilo.novo_obvestilo,
        name = 'novo_obvestilo'),
    path('obvestilo/<mhash:key>/brisi/<int:obvestilo_id>', views.obvestilo.del_obvestilo,
        name = 'del_obvestilo'),

    path('qr_kontrole/<mhash:key>/', views.qr.kontrole, name = 'qr_kontrole'),
    path('qr_ekipe/<mhash:key>/', views.qr.ekipe, name = 'qr_ekipe'),

    path('<ehash:key>/', views.ekipa.index, name = 'ekipa_index'),
    path('obvestila/<ehash:key>/', views.ekipa.obvestila,
        name = 'ekipa_obvestila'),
    path('obvestila/<ehash:key>/mark_read/<int:obvestilo_id>',
        views.ekipa.mark_read, name = 'mark_read'),
    path('info/<ehash:key>/', views.ekipa.info, name = 'ekipa_info'),
]
