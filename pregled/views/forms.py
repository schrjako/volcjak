from django import forms


class DownloadTekmovanjeForm(forms.Form):

    Dan = forms.BooleanField(initial=True, required=False)
    Kategorija = forms.BooleanField(initial=True, required=False)
    KT = forms.BooleanField(label="KT", initial=True, required=False)
    Proga = forms.BooleanField(initial=True, required=False)
    Kontrola = forms.BooleanField(initial=True, required=False)
    Ekipa = forms.BooleanField(initial=True, required=False)
    Naloga = forms.BooleanField(initial=True, required=False)
    Rezultat = forms.BooleanField(initial=False, required=False)
    pretty = forms.BooleanField(initial=False, required=False)


class UploadTekmovanjeForm(forms.Form):

    action = forms.IntegerField(initial=0,
            widget=forms.Select(choices=[[0, "zamenjaj"], [1, "dodaj"]]))
    struktura_file = forms.FileField(label="Strukturna datoteka")
