from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from django.template import loader

from struktura.models import Tekmovanje, KT, Ekipa, Naloga


def kontrole(request, key):
    context = {'key': key, 'request': request}
    context['tekmovanje'] = tekmovanje = get_object_or_404(Tekmovanje, pk = key)
    context['elementi'] = []
    for kt in KT.objects.filter(dan__tekmovanje = key):
        if Naloga.objects.filter(kontrola__kt = kt).exists():
            context['elementi'].append(kt)
    context['view_name'] = 'struktura:rezultat'
    context['title_name'] = 'kontrole'
    return render(request, 'pregled/qr_seznam.html', context)


def ekipe(request, key):
    context = {'key': key, 'request': request}
    context['tekmovanje'] = tekmovanje = get_object_or_404(Tekmovanje, pk = key)
    context['elementi'] = Ekipa.objects.filter(kategorija__tekmovanje = tekmovanje)
    context['view_name'] = 'pregled:ekipa_index'
    context['title_name'] = 'ekipe'
    return render(request, 'pregled/qr_seznam.html', context)
