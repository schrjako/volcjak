from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from django.template import loader

from struktura.models import Tekmovanje, KT, Kategorija, Ekipa, Proga, Kontrola, Naloga


def index(request, key):
    context = {'key': key}
    context['tekmovanje'] = get_object_or_404(Tekmovanje, pk=key)
    template = loader.get_template('pregled/index.html')
    return HttpResponse(template.render(context, request))


def kontrole(request, key):
    context = {'key': key}
    context['tekmovanje'] = get_object_or_404(Tekmovanje, pk=key)
    context['kontrole'] = Kontrola.objects.filter(kt__dan__tekmovanje=key)
    return render(request, 'pregled/kontrole.html', context)


def proge(request, key):
    context = {'key': key}
    context['tekmovanje'] = get_object_or_404(Tekmovanje, pk=key)
    context['proge'] = Proga.objects.filter(kategorija__tekmovanje=key)
    template = loader.get_template('pregled/proge.html')
    return HttpResponse(template.render(context, request))


def ekipe(request, key):
    context = {'key': key}
    context['tekmovanje'] = get_object_or_404(Tekmovanje, pk=key)
    context['kategorije'] = Kategorija.objects.filter(tekmovanje=key)
    template = loader.get_template('pregled/ekipe.html')
    return HttpResponse(template.render(context, request))


def rezultati(request, key):
    context = {'key': key}
    context['tekmovanje'] = tekmovanje = get_object_or_404(Tekmovanje, pk=key)
    context['kategorije'] = tekmovanje.kategorija_set.all()
    template = loader.get_template('pregled/rezultati.html')
    return HttpResponse(template.render(context, request))


def rezultati_v(request, key):
    context = {'key': key}
    context['tekmovanje'] = tekmovanje = get_object_or_404(Tekmovanje, pk=key)
    context['kategorije'] = tekmovanje.kategorija_set.all()
    template = loader.get_template('pregled/rezultati_v.html')
    return HttpResponse(template.render(context, request))
