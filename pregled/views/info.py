from django.http import HttpResponse, Http404, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.template import loader
from django import forms
from django.urls import reverse

from struktura.models import Tekmovanje, Ekipa

info_imena = ["topotesti", "vrisovanje", "morse", "spanje"]


class InfoForm(forms.Form):

    topotesti = forms.CharField(required = False)
    vrisovanje = forms.CharField(required = False)
    morse = forms.CharField(required = False)
    spanje = forms.CharField(required = False)


def default_context(key):
    context = {'key': key}
    context['tekmovanje'] = get_object_or_404(Tekmovanje, pk = key)
    context['info_imena'] = info_imena
    return context


def index(request, key):
    context = default_context(key)
    context['kategorije'] = context['tekmovanje'].kategorija_set.all()
    context['request'] = request
    template = loader.get_template('pregled/info.html')
    return HttpResponse(template.render(context, request))


def ekipa(request, key, ekipa_id):
    context = default_context(key)
    context['kategorije'] = context['tekmovanje'].kategorija_set.all()
    context['ekipa'] = ekipa = get_object_or_404(Ekipa, pk = ekipa_id)
    if ekipa.kategorija.tekmovanje != context['tekmovanje']:
        raise Http404('Chosen ekipa is not of the same tekmovanje as your link!')
    if request.method == 'POST':
        form = InfoForm(data = request.POST)
        if form.is_valid():
            ekipa.info = form.cleaned_data
            ekipa.save()
            return HttpResponseRedirect(reverse('pregled:info_index', args = [key]))
    else:
        form = InfoForm(data = ekipa.info)
    context['form'] = form
    template = loader.get_template('pregled/info_ekipa.html')
    return HttpResponse(template.render(context, request))


def view(request, key, ime = 'topotesti'):
    context = default_context(key)
    context['cime'] = ime
    values = {}
    for ekipa in Ekipa.objects.filter(kategorija__tekmovanje = key):
        if (ekipa.info == None) or (not ime in ekipa.info.keys()) or (ekipa.info[ime] == ''):
            continue
        if not ekipa.info[ime] in values.keys():
            values[ekipa.info[ime]] = []
        values[ekipa.info[ime]].append(ekipa)
    context['values'] = values
    template = loader.get_template('pregled/info_view.html')
    return HttpResponse(template.render(context, request))
