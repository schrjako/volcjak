from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404
from django.template import loader

from struktura.models import Tekmovanje, Ekipa
from ..models import Obvestilo
from .info import info_imena


def default_context(key):
    context = {'key': key}
    context['ekipa'] = get_object_or_404(Ekipa, pk = key)
    context['tekmovanje'] = context['ekipa'].kategorija.tekmovanje
    context['read'] = context['ekipa'].st_prebranih_obvestil
    context['unread'] = context['ekipa'].st_neprebranih_obvestil
    context['st_obvestil'] = context['tekmovanje'].st_obvestil
    return context


def index(request, key):
    context = default_context(key)
    context['request'] = request
    template = loader.get_template('pregled/ekipa_index.html')
    return HttpResponse(template.render(context, request))


def obvestila(request, key):
    context = default_context(key)
    context['obvestila'] = context['tekmovanje'].obvestilo_set.filter(visible = True)
    template = loader.get_template('pregled/ekipa_obvestila.html')
    return HttpResponse(template.render(context, request))


def mark_read(request, key, obvestilo_id):
    ekipa = get_object_or_404(Ekipa, pk = key)
    obvestilo = get_object_or_404(Obvestilo, pk = obvestilo_id)
    obvestilo.ekipe.add(ekipa)
    return HttpResponse("success!")


def info(request, key):
    context = {'key': key, 'info_imena': info_imena}
    context['ekipa'] = ekipa = get_object_or_404(Ekipa, pk = key)
    context['tekmovanje'] = tekmovanje = ekipa.kategorija.tekmovanje
    template = loader.get_template('pregled/ekipa_info.html')
    return HttpResponse(template.render(context, request))
