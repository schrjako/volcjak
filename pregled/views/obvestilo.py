from django.http import HttpResponseRedirect, HttpResponse, Http404
from django.shortcuts import get_object_or_404
from django.template import loader
from django import forms
from django.urls import reverse

from struktura.models import Tekmovanje
from ..models import Obvestilo


class ObvestiloForm(forms.ModelForm):

    class Meta:
        model = Obvestilo
        exclude = ['tekmovanje', 'cas_objave', 'ekipe']
        widgets = {
            'besedilo': forms.Textarea(),
        }


def obvestila(request, key):
    context = {'key': key}
    context['tekmovanje'] = tekmovanje = get_object_or_404(Tekmovanje, pk = key)
    context['obvestila'] = Obvestilo.objects.filter(tekmovanje = key)
    template = loader.get_template('pregled/obvestila.html')
    return HttpResponse(template.render(context, request))


def obvestilo(request, key, obvestilo_id):
    context = {'key': key, 'obvestilo_id': obvestilo_id}
    context['tekmovanje'] = tekmovanje = get_object_or_404(Tekmovanje, pk = key)
    context['obvestilo'] = obvestilo = get_object_or_404(Obvestilo, pk = obvestilo_id)
    if obvestilo.tekmovanje != tekmovanje:
        raise Http404('Chosen obvestilo id is not of the same tekmovanje as hash!')
    if request.method == 'POST':
        form = ObvestiloForm(data = request.POST, instance = obvestilo)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect(reverse('pregled:obvestila', args = [key]))
    else:
        form = ObvestiloForm(instance = obvestilo)
    context['form'] = form
    template = loader.get_template('pregled/obvestilo.html')
    return HttpResponse(template.render(context, request))


def novo_obvestilo(request, key):
    context = {'key': key}
    context['tekmovanje'] = tekmovanje = get_object_or_404(Tekmovanje, pk = key)

    if request.method == 'POST':
        form = ObvestiloForm(data = request.POST)
        if form.is_valid():
            obvestilo = form.save(commit = False)
            obvestilo.tekmovanje = Tekmovanje.objects.get(pk = key)
            obvestilo.save()
            return HttpResponseRedirect(reverse('pregled:obvestila', args=[key]))
    else:
        form = ObvestiloForm(data = {'tekmovanje_id': key})
    context['form'] = form
    template = loader.get_template('pregled/novo_obvestilo.html')
    return HttpResponse(template.render(context, request))


def del_obvestilo(request, key, obvestilo_id):
    tekmovanje = get_object_or_404(Tekmovanje, pk = key)
    obvestilo = get_object_or_404(Obvestilo, pk = obvestilo_id)
    if obvestilo.tekmovanje != tekmovanje:
        raise Http404('Chosen obvestilo id is not of the same tekmovanje as hash!')
    obvestilo.delete()
    return HttpResponseRedirect(reverse('pregled:obvestila', args=[key]))
