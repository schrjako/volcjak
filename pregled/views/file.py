import json

from django.urls import reverse
from django.http import HttpResponse, FileResponse, Http404, HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.core.files.base import ContentFile

from struktura.models import Tekmovanje
from .forms import DownloadTekmovanjeForm, UploadTekmovanjeForm


def import_export(request, key):
    context = {'key': key}
    context['tekmovanje'] = tekmovanje = get_object_or_404(Tekmovanje, pk = key)
    context['download_form'] = DownloadTekmovanjeForm()
    context['upload_form'] = UploadTekmovanjeForm()
    return render(request, 'pregled/import_export.html', context)


def download(request, key):
    tekmovanje = get_object_or_404(Tekmovanje, pk = key)
    form = DownloadTekmovanjeForm(request.GET)
    if not form.is_valid():
        raise Http404("Form is invalid!")
    indent = '\t' if form.cleaned_data['pretty'] else ''
    file = ContentFile(json.dumps(tekmovanje.get_tekmovanje(form.cleaned_data), indent=indent).encode())
    response = HttpResponse(file, 'application/json')
    response['Content-Length'] = file.size
    response['Content-Disposition'] = 'attachment; filename="volcjak_{}_{}.json"'.format(tekmovanje.leto, tekmovanje.ime.replace(' ', '_'))
    return response


def upload(request, key):
    tekmovanje = get_object_or_404(Tekmovanje, pk = key)
    form = UploadTekmovanjeForm(request.POST, request.FILES)
    if not form.is_valid():
        raise Http404(f"Form is invalid! {form.errors}")
    obj = json.load(form.cleaned_data['struktura_file'])
    if form.cleaned_data['action'] == 0:
        tekmovanje.remove_children()
    tekmovanje.make_tekmovanje(obj)
    return HttpResponseRedirect(reverse('pregled:import_export', args=[tekmovanje.pk]))
