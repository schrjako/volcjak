from django.db import models
from django.utils import timezone

from struktura.models import Tekmovanje, Kategorija, Ekipa


class Obvestilo(models.Model):

    tekmovanje = models.ForeignKey(Tekmovanje, on_delete = models.CASCADE)
    naslov = models.CharField(max_length = 50)
    besedilo = models.CharField(max_length = 500)
    visible = models.BooleanField('objavljeno', default = False)
    cas_vnosa = models.DateTimeField('čas vnosa', auto_now_add = True)
    cas_objave = models.DateTimeField('čas objave', null = True)
    ekipe = models.ManyToManyField(Ekipa, blank = True)

    class Meta:
        ordering = ['tekmovanje', '-visible', '-cas_objave', '-cas_vnosa']

    def save(self):
        if self.visible:
            self.cas_objave = timezone.now()
        else:
            self.cas_objave = None
        super().save()

    def __str__(self):
        fmt_str = "%d. %m. %H:%M"
        if self.visible:
            return self.tekmovanje.ime + ' objavljeno: ' +\
                    timezone.localtime(self.cas_objave).strftime(fmt_str) +\
                    " " + self.naslov[:15]
        return self.tekmovanje.ime + ' neobjavljeno: ' +\
                timezone.localtime(self.cas_vnosa).strftime(fmt_str) +\
                " " + self.naslov[:15]

    @property
    def st_ekip(self):
        return self.ekipe.count()
