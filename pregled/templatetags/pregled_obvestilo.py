from django import template

register = template.Library()

@register.filter
def read(obvestilo, ekipa_id):
    return obvestilo.ekipe.filter(pk = ekipa_id).exists()

@register.filter
def izloci(qs1, qs2):
    return qs1.exclude(pk__in = qs2)
