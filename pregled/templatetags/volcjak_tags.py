from django import template
from django.urls import reverse

register = template.Library()

@register.filter
def reverse_f(key, target):
    return reverse(target, args = [key])

@register.filter
def full_url(path, request):
    return request.build_absolute_uri(path)

@register.filter
def dict_lookup(obj, name):
    if (obj == None) or (not name in obj.keys()):
        return ""
    return obj[name]
