function make_cell(row, type, id, text){
	var cell;
	if((cell = document.getElementById(id)) == null){
		cell = document.createElement(type);
		cell.id = id;
		row.append(cell);
	}
	cell.innerHTML = text == undefined ? "" : text;
}

function make_title_row(table, tab){
	var ttrid = "ttr-id-" + tab.pk;
	var row;
	if((row = document.getElementById(ttrid)) == null){
		row = document.createElement("tr");
		row.id = ttrid;
		table.append(row);
	}
	for(var col of tab['cols']){
		text = ('url' in col) ? '<a class="plain" href="' + col.url + '">' + col.ime + '</a>' : col.ime;
		make_cell(row, "th", "th-id-" + tab.pk + "-" + col.pk, text);
	}
	return row;
}

function make_row(table, tab, row_def){
	var rid = "tr-id-" + tab.pk + "-" + row_def.pk;
	var row;
	if((row = document.getElementById(rid)) == null){
		row = document.createElement("tr");
		row.id = rid;
		table.append(row);
	}
	if(row_def.obscur) row.classList.add("disqualified");
	for(var col of tab['cols']){
		var type = col['head'] ? "th" : "td";
		if('urls' in row_def && col.pk in row_def.urls){
			text = '<a class="plain" href="' + row_def.urls[col.pk] + '">' + row_def.values[col.pk] + '</a>';
		}
		else{
			text = row_def.values[col.pk];
		}
		make_cell(row, type, type + "-id-" + tab.pk + "-" + row_def.pk
				+ "-" + col.pk, text);
	}
}

function make_table(div, tab){
	var tid = "t-id-" + tab.pk;
	var table;
	$("#" + tid).remove();
	if((table = document.getElementById(tid)) == null){
		table = document.createElement("table");
		table.id = tid;
		div.append(table);
	}
	make_title_row(table, tab);
	for(var row_def of Object.values(tab['rows'])){
		make_row(table, tab, row_def);
	}
}
