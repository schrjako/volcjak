from django.test import TestCase
from struktura.models import Tekmovanje


class TekmovanjeTestCase(TestCase):
    fixtures = ['fixture1']

    def setUp(self):
        pass

    def test_ordering(self):
        tekmovanjes = Tekmovanje.objects.all()
        tprev = None
        for t in tekmovanjes:
            if tprev is not None:
                self.assertTrue(tprev.leto < t.leto or
                                (tprev.leto == t.leto and tprev.ime < t.ime))

    def test_str(self):
        self.assertQuerysetEqual(Tekmovanje.objects.all(), ['<Tekmovanje: NOT 2021>', '<Tekmovanje: ROT 2021>', '<Tekmovanje: ZOT 2021>', '<Tekmovanje: ROT 2022>', '<Tekmovanje: ROT 2023>', '<Tekmovanje: ROT 2025>'])
