from django.shortcuts import render, get_object_or_404
from django import forms

from ..models import Tekmovanje


class TekmovanjeForm(forms.ModelForm):

    class Meta:
        model = Tekmovanje
        exclude = []


def v_tekmovanje(request, key):
    context = {'sec_name': 'tekmovanje', 'key': key}
    context['tekmovanje'] = tekmovanje = get_object_or_404(Tekmovanje, pk=key)
    if request.method == 'POST':
        form = TekmovanjeForm(data=request.POST, instance=tekmovanje)
        if form.is_valid():
            form.save()
    else:
        form = TekmovanjeForm(instance=tekmovanje)
    context['form'] = form
    return render(request, 'struktura/tekmovanje.html', context)
