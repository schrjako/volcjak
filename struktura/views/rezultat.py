from django.shortcuts import render, get_object_or_404
from django import forms
from django.http import Http404, HttpResponseRedirect, HttpResponse
from django.urls import reverse
from django.db import transaction, IntegrityError

from datetime import timedelta, datetime

from ..models import Tekmovanje, Kategorija, Kontrola, KT, Proga, Ekipa
from ..models import Naloga, NalNum, NalHitS, NalHitC, NalProS, NalProC
from ..models import Rezultat, RezHitS, RezHitC, RezProS, RezProC


def get_eki_pks(kt_key, kat_key):
    return list(map(lambda ekipa: ekipa.pk,
                    filter(lambda ekipa: Rezultat.objects.filter(ekipa=ekipa,
                                                                 naloga__kontrola__kt=kt_key).exists(),
                           Ekipa.objects.filter(kategorija=kat_key))))


def get_baseForm(kt_key, kat_key=None, eki_key=None):
    if eki_key is None:
        eki_qs = Ekipa.objects.filter(kategorija=kat_key).exclude(pk__in=get_eki_pks(kt_key, kat_key))

        class baseForm(forms.Form):
            ekipa = forms.ModelChoiceField(queryset=eki_qs)
    else:
        class baseForm(forms.Form):
            ekipa = forms.ModelChoiceField(queryset=Ekipa.objects.filter(pk=eki_key), disabled=True, initial=Ekipa.objects.get(pk=eki_key))

    class baseForm(baseForm):
        mrtvi_cas = forms.DurationField(label='mrtvi čas')
        cas = forms.SplitDateTimeField()
        prefix = 'base'
    return baseForm


def default_context(key):
    return {'key': key}


def get_naloge(kt, kategorija):
    nals = Naloga.objects.filter(kontrola__kt=kt, kontrola__proga__kategorija=kategorija)
    return list(map(lambda naloga: naloga.child_obj, nals))


def v_rezultat(request, key):
    context = default_context(key)
    context['kt'] = kt = get_object_or_404(KT, pk=key)
    context['rezultats'] = rezultats = Rezultat.objects.filter(naloga__kontrola__kt=key)
    context['kontrolas'] = Kontrola.objects.filter(kt=kt)
    if context['kontrolas'].count() > 0:
        context['kontrola'] = context['kontrolas'].first()
    context['kategorijas'] = []
    for kategorija in Kategorija.objects.filter(tekmovanje=kt.tekmovanje):
        if context['kontrolas'].filter(proga__kategorija=kategorija).count() > 0:
            eki_pks = get_eki_pks(key, kategorija.pk)
            context['kategorijas'].append((kategorija, [], len(eki_pks)==kategorija.ekipa_set.all().count()))
            for ekipa in kategorija.ekipa_set.filter(pk__in=eki_pks):
                context['kategorijas'][-1][1].append(ekipa)
    return render(request, 'struktura/rezultat.html', context)


def v_rezultat_edit(request, key, eki_key):
    context = default_context(key)
    context['kt'] = kt = get_object_or_404(KT, pk=key)
    context['ekipa'] = ekipa = get_object_or_404(Ekipa, pk=eki_key)
    context['kategorija'] = kategorija = ekipa.kategorija
    context['kontrola'] = kontrola = kt.kontrola_set.get(proga__kategorija=kategorija)
    context['naloge'] = naloge = get_naloge(kt, kategorija)
    context['submit_buttons'] = [
            {'val': 'shrani', 'name': 'save'},
            {'val': 'shrani in zapri', 'name': 'save_and_exit'},
            {'val': 'prekliči', 'name': 'cancel'},
            ]
    formsets = []
    base_form_class = get_baseForm(key, eki_key=eki_key)
    if not ekipa.rezultat_set.filter(naloga__in=naloge).exists():
        return HttpResponseRedirect(reverse('struktura:rezultat_add', args=[key, ekipa.kategorija.pk]))
    if request.method == 'POST':
        if 'cancel' in request.POST:
            return HttpResponseRedirect(reverse('struktura:rezultat', args=[key]))
        context['base_form'] = base_form = base_form_class(data=request.POST)
        are_valid = True
        for naloga in naloge:
            initial_rez = list(map(lambda rezultat: rezultat.child_obj.to_initial(naloga),
                            naloga.rezultat_set.filter(ekipa=ekipa).order_by('pk')))
            formset = naloga.rez_formset(data=request.POST, initial=initial_rez)
            formset.empty_permitted = False
            formsets.append((naloga, formset))
            if not formset.is_valid():
                are_valid = False
        if base_form.is_valid() and are_valid:
            mrtvi_cas = base_form.cleaned_data['mrtvi_cas']
            cas = base_form.cleaned_data['cas']
            with transaction.atomic():
                for naloga, formset in formsets:
                    if not formset.has_changed() and not base_form.has_changed():
                        continue

                    formset.save(mrtvi_cas, cas)
            if 'mrtvi_cas' in base_form.changed_data:
                naloge[0].kontrola.proga.calculate_casovnica(ekipa=ekipa.pk)
            if 'save' in request.POST:
                return HttpResponseRedirect(
                        reverse('struktura:rezultat_edit', args=[key, eki_key]))
            if 'save_and_exit' in request.POST:
                from pprint import pprint
                pprint(request.POST)
                pprint(request.META)
                return HttpResponseRedirect( reverse('struktura:rezultat',
                                                     args=[key]) +
                                            f'#tabs-{kategorija.pk}')
    else:
        for naloga in naloge:
            initial_rez = list(map(lambda rezultat: rezultat.child_obj.to_initial(naloga),
                            naloga.rezultat_set.filter(ekipa=ekipa).order_by('pk')))
            formsets.append((naloga, naloga.rez_formset(initial=initial_rez)))
        example_rez = ekipa.rezultat_set.filter(naloga__in=naloge).first()
        mrtvi_cas = example_rez.mrtvi_cas
        cas = example_rez.cas
        context['base_form'] = base_form_class(initial={'mrtvi_cas': mrtvi_cas, 'cas': cas})
    context['formsets'] = formsets

    return render(request, 'struktura/rezultat_edit.html', context)


def v_rezultat_add(request, key, kat_key):
    context = default_context(key)
    context['kt'] = kt = get_object_or_404(KT, pk=key)
    context['kategorija'] = kategorija = get_object_or_404(Kategorija, pk=kat_key)
    context['naloge'] = naloge = get_naloge(kt, kategorija)
    if len(naloge) > 0:
        context['kontrola'] = naloge[0].kontrola
    formsets = []
    base_form_class = get_baseForm(key, kat_key=kat_key)
    if request.method == 'POST':
        if 'cancel' in request.POST:
            return HttpResponseRedirect(reverse('struktura:rezultat', args=[key]))
        context['base_form'] = base_form = base_form_class(data=request.POST)
        are_valid = True
        for naloga in naloge:
            formset = naloga.rez_formset(data=request.POST)
            formsets.append((naloga, formset))
            if not formset.is_valid():
                are_valid = False
        if base_form.is_valid() and are_valid:
            ekipa = base_form.cleaned_data['ekipa']
            cas = base_form.cleaned_data['cas']
            mrtvi_cas = base_form.cleaned_data['mrtvi_cas']
            with transaction.atomic():
                for naloga, formset in formsets:
                    #   In the case of already existing Rezultats, this request
                    #   cannot be an add request, and should be an edit request.
                    if Rezultat.objects.filter(naloga=naloga, ekipa=ekipa).count() > 0:
                        raise IntegrityError('Rezultat for {} exists but should not!'.format(str(naloga)))
                    naloga.add_rezultats(ekipa, mrtvi_cas, cas, formset)
            if mrtvi_cas:
                naloge[0].kontrola.proga.calculate_casovnica(ekipa=ekipa.pk)
            if 'save_and_exit' in request.POST:
                return HttpResponseRedirect(reverse('struktura:rezultat', args=[key]) +
                                            f'#tabs-{kategorija.pk}')
            if 'save_and_edit' in request.POST:
                return HttpResponseRedirect(
                        reverse('struktura:rezultat_edit', args=[key, ekipa.pk]))
            if 'save_and_add_another' in request.POST:
                return HttpResponseRedirect(reverse('struktura:rezultat_add', args=[key, kat_key]))
            #   save_and_add_similar needs updated ekipa queryset
            base_form_class = get_baseForm(key, kat_key=kat_key)
            context['base_form'] = base_form = base_form_class(initial={'mrtvi_cas': mrtvi_cas, 'cas': cas})

    else:
        for naloga in naloge:
            formsets.append((naloga, naloga.rez_formset()))
        context['base_form'] = base_form_class(initial={'mrtvi_cas': timedelta(0), 'cas': datetime.now})
    context['formsets'] = formsets

    context['submit_buttons'] = [
            {'val': 'shrani in zapri', 'name': 'save_and_exit'},
            {'val': 'shrani in uredi', 'name': 'save_and_edit'},
            ]
    eki_pks = get_eki_pks(key, kat_key)
    if len(eki_pks) == kategorija.ekipa_set.all().count():
        return HttpResponseRedirect(reverse('struktura:rezultat', args=[key]))
    if len(eki_pks) < kategorija.ekipa_set.all().count() - 1:
        context['submit_buttons'].extend([
                {'val': 'shrani in dodaj novo', 'name': 'save_and_add_another'},
                {'val': 'shrani in dodaj podobno', 'name': 'save_and_add_similar'},
                ])
    context['submit_buttons'].append({'val': 'prekliči', 'name': 'cancel'})

    return render(request, 'struktura/rezultat_add.html', context)


def v_rezultat_rm(request, key, eki_key):
    ekipa = get_object_or_404(Ekipa, pk=eki_key)
    qs_rez = Rezultat.objects.filter(ekipa=ekipa, naloga__kontrola__kt=key)
    #   TODO calculate points on delete
    if qs_rez.exists() and qs_rez.first().mrtvi_cas:
        qs_proga = Proga.objects.filter(kategorija=ekipa.kategorija, start__dan=qs_rez.first().dan)
        num, _ = qs_rez.delete()
        for proga in qs_proga:
            proga.calculate_casovnica(ekipa=eki_key)
    else:
        num, _ = qs_rez.delete()
    return HttpResponse('Successfully deleted {} objects of type Rezultat (or some of its subtypes)'.format(str(num)), status=200)
