from django.shortcuts import render, get_object_or_404
from django.http import Http404, HttpResponseRedirect, HttpResponse
from django.urls import reverse
from django.db.models import deletion
from django.db import transaction

from ..models import Tekmovanje, Kontrola, Naloga, NalNum, NalHitS, NalHitC


nal_type_to_obj = Naloga.nal_type_to_obj()


def default_context(key):
    return {'key': key, 'sec_name': 'naloga', 'display_name': 'Naloga',
            'plural_name': 'Naloge'}


def v_naloga(request, key):
    context = default_context(key)
    context['tekmovanje'] = get_object_or_404(Tekmovanje, pk=key)
    context['objs'] = Naloga.objects.filter(kontrola__proga__kategorija__tekmovanje=key)
    return render(request, 'struktura/index.html', context)


def v_naloga_edit(request, key, obj_key):
    context = default_context(key)
    context['tekmovanje'] = tekmovanje = get_object_or_404(Tekmovanje, pk=key)
    naloga = get_object_or_404(Naloga, pk=obj_key)
    context['submit_buttons'] = [
            {'val': 'shrani', 'name': 'save'},
            {'val': 'shrani in zapri', 'name': 'save_and_exit'},
            {'val': 'prekliči', 'name': 'cancel'}
            ]
    if naloga.tekmovanje != tekmovanje:
        raise Http404("Naloga not of right tekmovanje!")
    qs = Kontrola.objects.filter(kt__dan__tekmovanje=key)
    naloga = naloga.child_obj
    nal_type = type(naloga).__name__

    if request.method == 'POST':
        if 'cancel' in request.POST:
            return HttpResponseRedirect(reverse('struktura:naloga', args=[key]))
        form = nal_type_to_obj[nal_type].form(qs)(data=request.POST, instance=naloga)
        if form.is_valid():
            with transaction.atomic():
                form.save()
            if 'save_and_exit' in request.POST:
                return HttpResponseRedirect(reverse('struktura:naloga', args=[key]))
    else:
        form = nal_type_to_obj[nal_type].form(qs)(instance=naloga)
    context['obj'] = naloga
    context['nal_type'] = nal_type
    context['form'] = form
    return render(request, 'struktura/naloga_edit.html', context)


def v_naloga_add(request, key):
    context = default_context(key)
    context['tekmovanje'] = tekmovanje = get_object_or_404(Tekmovanje, pk=key)
    context['submit_buttons'] = [
            {'val': 'shrani in zapri', 'name': 'save_and_exit'},
            {'val': 'shrani in uredi', 'name': 'save_and_edit'},
            {'val': 'shrani in dodaj novo', 'name': 'save_and_add_another'},
            {'val': 'shrani in dodaj podobno', 'name': 'save_and_add_similar'},
            {'val': 'prekliči', 'name': 'cancel'}
            ]
    qs = Kontrola.objects.filter(kt__dan__tekmovanje=key)
    forms = {nal_type: nalClass.form(qs) for nal_type, nalClass in nal_type_to_obj.items() if nalClass.has_form()}
    context['default_nal_type'] = Naloga.__name__
    if request.method == 'POST':
        if 'cancel' in request.POST:
            return HttpResponseRedirect(reverse('struktura:naloga', args=[key]))
        if 'nal_type' not in request.POST:
            nal_type = 'naloga'
        if request.POST['nal_type'] not in nal_type_to_obj:
            return HttpResponse('Naloga of type {} does not exist!'.format(nal_type), status=400)
        nal_type = request.POST['nal_type']
        form = nal_type_to_obj[nal_type].form(qs)(data=request.POST)
        if form.is_valid():
            naloga = form.save()
            if 'save_and_exit' in request.POST:
                return HttpResponseRedirect(
                        reverse('struktura:naloga', args=[key]))
            if 'save_and_edit' in request.POST:
                return HttpResponseRedirect(
                        reverse('struktura:naloga_edit', args=[key, naloga.pk]))
            if 'save_and_add_another' in request.POST:
                return HttpResponseRedirect(
                        reverse('struktura:naloga_add', args=[key]))
        context['default_nal_type'] = nal_type
        forms[nal_type] = form
    context['forms'] = forms
    return render(request, 'struktura/naloga_add.html', context)


def v_naloga_rm(request, key, obj_key):
    naloga = get_object_or_404(Naloga, pk=obj_key)
    if naloga.kontrola.proga.kategorija.tekmovanje.pk == key:
        try:
            naloga.child_obj.delete()
            return HttpResponse('Successfully deleted Naloga [{}]!'.format(str(naloga)), status=200)
        except deletion.RestrictedError:
            return HttpResponse('Naloga [{}] not deleted because of restricted foreign keys!'.format(str(naloga)), status=500)
    return HttpResponse('Naloga does not match tekmovanje pk, deletion refused!', status=403)
