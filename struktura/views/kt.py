from django.db.models import deletion
from django.shortcuts import render, get_object_or_404
from django import forms
from django.http import Http404, HttpResponseRedirect, HttpResponse
from django.urls import reverse

from ..models import Tekmovanje, Dan, KT


def get_KTForm(qs):
    class KTForm(forms.ModelForm):
        dan = forms.ModelChoiceField(queryset=qs)

        class Meta:
            model = KT
            exclude = ['vrstni_red']
    return KTForm


def default_context(key):
    return {'key': key, 'sec_name': 'kt', 'display_name': 'KT',
            'plural_name': 'KT-ji'}


def v_kt(request, key):
    context = default_context(key)
    context['tekmovanje'] = get_object_or_404(Tekmovanje, pk=key)
    context['objs'] = KT.objects.filter(dan__tekmovanje=key)
    return render(request, 'struktura/index.html', context)


def v_kt_edit(request, key, obj_key):
    context = default_context(key)
    context['tekmovanje'] = tekmovanje = get_object_or_404(Tekmovanje, pk=key)
    context['obj'] = kt = get_object_or_404(KT, pk=obj_key)
    context['submit_buttons'] = [
            {'val': 'shrani', 'name': 'save'},
            {'val': 'shrani in zapri', 'name': 'save_and_exit'},
            {'val': 'prekliči', 'name': 'cancel'}
            ]
    if kt.tekmovanje != tekmovanje:
        raise Http404("KT not of right tekmovanje!")
    qs = Dan.objects.filter(tekmovanje=key)
    if request.method == 'POST':
        if 'cancel' in request.POST:
            return HttpResponseRedirect(reverse('struktura:kt', args=[key]))
        form = get_KTForm(qs)(data=request.POST, instance=kt)
        if form.is_valid():
            form.save()
            if 'save_and_exit' in request.POST:
                return HttpResponseRedirect(reverse('struktura:kt', args=[key]))
    else:
        form = get_KTForm(qs)(instance=kt)
    context['form'] = form
    return render(request, 'struktura/edit.html', context)


def v_kt_add(request, key):
    context = default_context(key)
    context['tekmovanje'] = tekmovanje = get_object_or_404(Tekmovanje, pk=key)
    context['submit_buttons'] = [
            {'val': 'shrani in zapri', 'name': 'save_and_exit'},
            {'val': 'shrani in uredi', 'name': 'save_and_edit'},
            {'val': 'shrani in dodaj novo', 'name': 'save_and_add_another'},
            {'val': 'shrani in dodaj podobno', 'name': 'save_and_add_similar'},
            {'val': 'prekliči', 'name': 'cancel'}
            ]
    qs = Dan.objects.filter(tekmovanje=key)
    if request.method == 'POST':
        if 'cancel' in request.POST:
            return HttpResponseRedirect(reverse('struktura:kt', args=[key]))
        form = get_KTForm(qs)(data=request.POST)
        if form.is_valid():
            kt = form.save(commit=False)
            kt.save()
            if 'save_and_exit' in request.POST:
                return HttpResponseRedirect(
                        reverse('struktura:kt', args=[key]))
            if 'save_and_edit' in request.POST:
                return HttpResponseRedirect(
                        reverse('struktura:kt_edit', args=[key, kt.pk]))
            if 'save_and_add_another' in request.POST:
                return HttpResponseRedirect(
                        reverse('struktura:kt_add', args=[key]))
            if 'save_and_add_similar' in request.POST:
                data = request.POST.copy()
                form = get_KTForm(qs)(data=data)
    else:
        form = get_KTForm(qs)()
    context['form'] = form
    return render(request, 'struktura/add.html', context)


def v_kt_rm(request, key, obj_key):
    kt = get_object_or_404(KT, pk=obj_key)
    if kt.tekmovanje.pk == key:
        try:
            kt.delete()
            return HttpResponse('Successfully deleted KT [{}]!'.format(str(kt)), status=200)
        except deletion.RestrictedError:
            return HttpResponse('KT [{}] not deleted because of restricted foreign keys!'.format(str(kt)), status=500)
    return HttpResponse('KT does not match tekmovanje pk, deletion refused!', status=403)
