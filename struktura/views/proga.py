from django.db.models import deletion
from django.shortcuts import render, get_object_or_404
from django.http import Http404, HttpResponseRedirect, HttpResponse
from django.urls import reverse

from ..models import Tekmovanje, Kategorija, KT, Proga


def default_context(key):
    return {'key': key, 'sec_name': 'proga', 'display_name': 'Proga',
            'plural_name': 'Proge'}


def v_proga(request, key):
    context = default_context(key)
    context['tekmovanje'] = get_object_or_404(Tekmovanje, pk=key)
    context['objs'] = Proga.objects.filter(kategorija__tekmovanje=key)
    return render(request, 'struktura/index.html', context)


def v_proga_edit(request, key, obj_key):
    context = default_context(key)
    context['tekmovanje'] = tekmovanje = get_object_or_404(Tekmovanje, pk=key)
    context['obj'] = proga = get_object_or_404(Proga, pk=obj_key)
    context['submit_buttons'] = [
            {'val': 'shrani', 'name': 'save'},
            {'val': 'shrani in zapri', 'name': 'save_and_exit'},
            {'val': 'prekliči', 'name': 'cancel'}
            ]
    if proga.tekmovanje != tekmovanje:
        raise Http404("Proga not of right tekmovanje!")
    kat_qs = Kategorija.objects.filter(tekmovanje=key)
    kt_qs = KT.objects.filter(dan__tekmovanje=key)
    if request.method == 'POST':
        if 'cancel' in request.POST:
            return HttpResponseRedirect(reverse('struktura:proga', args=[key]))
        form = Proga.form(kat_qs, kt_qs)(data=request.POST, instance=proga)
        if form.is_valid():
            form.save()
            if 'save_and_exit' in request.POST:
                return HttpResponseRedirect(reverse('struktura:proga', args=[key]))
    else:
        form = Proga.form(kat_qs, kt_qs)(instance=proga)
    context['form'] = form
    return render(request, 'struktura/edit.html', context)


def v_proga_add(request, key):
    context = default_context(key)
    context['tekmovanje'] = tekmovanje = get_object_or_404(Tekmovanje, pk=key)
    context['submit_buttons'] = [
            {'val': 'shrani in zapri', 'name': 'save_and_exit'},
            {'val': 'shrani in uredi', 'name': 'save_and_edit'},
            {'val': 'shrani in dodaj novo', 'name': 'save_and_add_another'},
            {'val': 'shrani in dodaj podobno', 'name': 'save_and_add_similar'},
            {'val': 'prekliči', 'name': 'cancel'}
            ]
    kat_qs = Kategorija.objects.filter(tekmovanje=key)
    kt_qs = KT.objects.filter(dan__tekmovanje=key)
    if request.method == 'POST':
        if 'cancel' in request.POST:
            return HttpResponseRedirect(reverse('struktura:proga', args=[key]))
        form = Proga.form(kat_qs, kt_qs)(data=request.POST)
        if form.is_valid():
            proga = form.save()
            if 'save_and_exit' in request.POST:
                return HttpResponseRedirect(
                        reverse('struktura:proga', args=[key]))
            if 'save_and_edit' in request.POST:
                return HttpResponseRedirect(
                        reverse('struktura:proga_edit', args=[key, proga.pk]))
            if 'save_and_add_another' in request.POST:
                return HttpResponseRedirect(
                        reverse('struktura:proga_add', args=[key]))
    else:
        form = Proga.form(kat_qs, kt_qs)()
    context['form'] = form
    return render(request, 'struktura/add.html', context)


def v_proga_rm(request, key, obj_key):
    proga = get_object_or_404(Proga, pk=obj_key)
    if proga.kategorija.tekmovanje.pk == key:
        try:
            proga.delete()
            return HttpResponse('Successfully deleted Proga [{}]!'.format(str(proga)), status=200)
        except deletion.RestrictedError:
            return HttpResponse('Proga [{}] not deleted because of restricted foreign keys!'.format(str(proga)), status=500)
    return HttpResponse('Proga does not match tekmovanje pk, deletion refused!', status=403)
