from django.db.models import deletion
from django.shortcuts import render, get_object_or_404
from django import forms
from django.http import Http404, HttpResponseRedirect, HttpResponse
from django.urls import reverse

from ..models import Tekmovanje, Kategorija, Ekipa


def get_EkipaForm(qs):
    class EkipaForm(forms.ModelForm):
        kategorija = forms.ModelChoiceField(queryset=qs)

        class Meta:
            model = Ekipa
            exclude = []
    return EkipaForm


def default_context(key):
    return {'key': key, 'sec_name': 'ekipa', 'display_name': 'Ekipa',
            'plural_name': 'Ekipe'}


def v_ekipa(request, key):
    context = default_context(key)
    context['tekmovanje'] = get_object_or_404(Tekmovanje, pk=key)
    context['objs'] = Ekipa.objects.filter(kategorija__tekmovanje=key)
    return render(request, 'struktura/index.html', context)


def v_ekipa_edit(request, key, obj_key):
    context = default_context(key)
    context['tekmovanje'] = tekmovanje = get_object_or_404(Tekmovanje, pk=key)
    context['obj'] = ekipa = get_object_or_404(Ekipa, pk=obj_key)
    context['submit_buttons'] = [
            {'val': 'shrani', 'name': 'save'},
            {'val': 'shrani in zapri', 'name': 'save_and_exit'},
            {'val': 'prekliči', 'name': 'cancel'}
            ]
    if ekipa.tekmovanje != tekmovanje:
        raise Http404("Ekipa not of right tekmovanje!")
    qs = Kategorija.objects.filter(tekmovanje=key)
    if request.method == 'POST':
        if 'cancel' in request.POST:
            return HttpResponseRedirect(reverse('struktura:ekipa', args=[key]))
        form = get_EkipaForm(qs)(data=request.POST, instance=ekipa)
        if form.is_valid():
            form.save()
            if 'save_and_exit' in request.POST:
                return HttpResponseRedirect(reverse('struktura:ekipa', args=[key]))
    else:
        form = get_EkipaForm(qs)(instance=ekipa)
    context['form'] = form
    return render(request, 'struktura/edit.html', context)


def v_ekipa_add(request, key):
    context = default_context(key)
    context['tekmovanje'] = tekmovanje = get_object_or_404(Tekmovanje, pk=key)
    context['submit_buttons'] = [
            {'val': 'shrani in zapri', 'name': 'save_and_exit'},
            {'val': 'shrani in uredi', 'name': 'save_and_edit'},
            {'val': 'shrani in dodaj novo', 'name': 'save_and_add_another'},
            {'val': 'shrani in dodaj podobno', 'name': 'save_and_add_similar'},
            {'val': 'prekliči', 'name': 'cancel'}
            ]
    qs = Kategorija.objects.filter(tekmovanje=key)
    if request.method == 'POST':
        if 'cancel' in request.POST:
            return HttpResponseRedirect(reverse('struktura:ekipa', args=[key]))
        form = get_EkipaForm(qs)(data=request.POST)
        if form.is_valid():
            ekipa = form.save(commit=False)
            ekipa.save()
            if 'save_and_exit' in request.POST:
                return HttpResponseRedirect(
                        reverse('struktura:ekipa', args=[key]))
            if 'save_and_edit' in request.POST:
                return HttpResponseRedirect(
                        reverse('struktura:ekipa_edit', args=[key, ekipa.pk]))
            if 'save_and_add_another' in request.POST:
                return HttpResponseRedirect(
                        reverse('struktura:ekipa_add', args=[key]))
            if 'save_and_add_similar' in request.POST:
                data = request.POST.copy()
                data['stevilka'] = int(data['stevilka']) + 1
                form = get_EkipaForm(qs)(data=data)
    else:
        form = get_EkipaForm(qs)()
    context['form'] = form
    return render(request, 'struktura/add.html', context)


def v_ekipa_rm(request, key, obj_key):
    ekipa = get_object_or_404(Ekipa, pk=obj_key)
    if ekipa.kategorija.tekmovanje.pk == key:
        try:
            ekipa.delete()
            return HttpResponse('Successfully deleted Ekipa [{}]!'.format(str(ekipa)), status=200)
        except deletion.RestrictedError:
            return HttpResponse('Ekipa [{}] not deleted because of restricted foreign keys!'.format(str(ekipa)), status=500)
    return HttpResponse('Ekipa does not match tekmovanje pk, deletion refused!', status=403)
