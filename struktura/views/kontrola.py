from django.db.models import deletion
from django.shortcuts import render, get_object_or_404
from django import forms
from django.http import Http404, HttpResponseRedirect, HttpResponse
from django.urls import reverse

from ..models import Tekmovanje, KT, Proga, Kontrola


def get_KontrolaForm(kt_qs, proga_qs):
    class KontrolaForm(forms.ModelForm):
        kt = forms.ModelChoiceField(queryset=kt_qs)
        proga = forms.ModelChoiceField(queryset=proga_qs)

        class Meta:
            model = Kontrola
            exclude = []
    return KontrolaForm


def default_context(key):
    return {'key': key, 'sec_name': 'kontrola', 'display_name': 'Kontrola',
            'plural_name': 'Kontrole'}


def v_kontrola(request, key):
    context = default_context(key)
    context['tekmovanje'] = get_object_or_404(Tekmovanje, pk=key)
    context['objs'] = Kontrola.objects.filter(proga__kategorija__tekmovanje=key)
    return render(request, 'struktura/index.html', context)


def v_kontrola_edit(request, key, obj_key):
    context = default_context(key)
    context['tekmovanje'] = tekmovanje = get_object_or_404(Tekmovanje, pk=key)
    context['obj'] = kontrola = get_object_or_404(Kontrola, pk=obj_key)
    context['submit_buttons'] = [
            {'val': 'shrani', 'name': 'save'},
            {'val': 'shrani in zapri', 'name': 'save_and_exit'},
            {'val': 'prekliči', 'name': 'cancel'}
            ]
    if kontrola.tekmovanje != tekmovanje:
        raise Http404("Kontrola not of right tekmovanje!")
    kt_qs = KT.objects.filter(dan__tekmovanje=key)
    proga_qs = Proga.objects.filter(kategorija__tekmovanje=key)
    if request.method == 'POST':
        if 'cancel' in request.POST:
            return HttpResponseRedirect(reverse('struktura:kontrola', args=[key]))
        form = get_KontrolaForm(kt_qs, proga_qs)(data=request.POST, instance=kontrola)
        if form.is_valid():
            form.save()
            if 'save_and_exit' in request.POST:
                return HttpResponseRedirect(reverse('struktura:kontrola', args=[key]))
    else:
        form = get_KontrolaForm(kt_qs, proga_qs)(instance=kontrola)
    context['form'] = form
    return render(request, 'struktura/edit.html', context)


def v_kontrola_add(request, key):
    context = default_context(key)
    context['tekmovanje'] = tekmovanje = get_object_or_404(Tekmovanje, pk=key)
    context['submit_buttons'] = [
            {'val': 'shrani in zapri', 'name': 'save_and_exit'},
            {'val': 'shrani in uredi', 'name': 'save_and_edit'},
            {'val': 'shrani in dodaj novo', 'name': 'save_and_add_another'},
            {'val': 'shrani in dodaj podobno', 'name': 'save_and_add_similar'},
            {'val': 'prekliči', 'name': 'cancel'}
            ]
    kt_qs = KT.objects.filter(dan__tekmovanje=key)
    proga_qs = Proga.objects.filter(kategorija__tekmovanje=key)
    if request.method == 'POST':
        if 'cancel' in request.POST:
            return HttpResponseRedirect(reverse('struktura:kontrola', args=[key]))
        form = get_KontrolaForm(kt_qs, proga_qs)(data=request.POST)
        if form.is_valid():
            kontrola = form.save(commit=False)
            kontrola.save()
            if 'save_and_exit' in request.POST:
                return HttpResponseRedirect(
                        reverse('struktura:kontrola', args=[key]))
            if 'save_and_edit' in request.POST:
                return HttpResponseRedirect(
                        reverse('struktura:kontrola_edit', args=[key, kontrola.pk]))
            if 'save_and_add_another' in request.POST:
                return HttpResponseRedirect(
                        reverse('struktura:kontrola_add', args=[key]))
    else:
        form = get_KontrolaForm(kt_qs, proga_qs)()
    context['form'] = form
    return render(request, 'struktura/add.html', context)


def v_kontrola_rm(request, key, obj_key):
    kontrola = get_object_or_404(Kontrola, pk=obj_key)
    if kontrola.tekmovanje.pk == key:
        try:
            kontrola.delete()
            return HttpResponse('Successfully deleted Kontrola [{}]!'.format(str(kontrola)), status=200)
        except deletion.RestrictedError:
            return HttpResponse('Kontrola [{}] not deleted because of restricted foreign keys!'.format(str(kontrola)), status=500)
    return HttpResponse('Kontrola does not match tekmovanje pk, deletion refused!', status=403)
