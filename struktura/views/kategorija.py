from django.db.models import deletion
from django.shortcuts import render, get_object_or_404
from django import forms
from django.http import Http404, HttpResponseRedirect, HttpResponse
from django.urls import reverse

from ..models import Tekmovanje, Kategorija


class KategorijaForm(forms.ModelForm):

    class Meta:
        model = Kategorija
        exclude = ['tekmovanje']


def default_context(key):
    return {'key': key, 'sec_name': 'kategorija', 'display_name': 'Kategorija',
            'plural_name': 'kategorije'}


def v_kategorija(request, key):
    context = default_context(key)
    context['tekmovanje'] = tekmovanje = get_object_or_404(Tekmovanje, pk=key)
    context['objs'] = tekmovanje.kategorija_set.all()
    return render(request, 'struktura/index.html', context)


def v_kategorija_edit(request, key, obj_key):
    context = default_context(key)
    context['tekmovanje'] = tekmovanje = get_object_or_404(Tekmovanje, pk=key)
    context['obj'] = kategorija = get_object_or_404(Kategorija, pk=obj_key)
    context['submit_buttons'] = [
            {'val': 'shrani', 'name': 'save'},
            {'val': 'shrani in zapri', 'name': 'save_and_exit'},
            {'val': 'prekliči', 'name': 'cancel'}
            ]
    if kategorija.tekmovanje != tekmovanje:
        raise Http404("Kategorija not of right tekmovanje!")
    if request.method == 'POST':
        if 'cancel' in request.POST:
            return HttpResponseRedirect(reverse('struktura:kategorija', args=[key]))
        form = KategorijaForm(data=request.POST, instance=kategorija)
        if form.is_valid():
            form.save()
            if 'save_and_exit' in request.POST:
                return HttpResponseRedirect(reverse('struktura:kategorija', args=[key]))
    else:
        form = KategorijaForm(instance=kategorija)
    context['form'] = form
    return render(request, 'struktura/edit.html', context)


def v_kategorija_add(request, key):
    context = default_context(key)
    context['tekmovanje'] = tekmovanje = get_object_or_404(Tekmovanje, pk=key)
    context['submit_buttons'] = [
            {'val': 'shrani in zapri', 'name': 'save_and_exit'},
            {'val': 'shrani in uredi', 'name': 'save_and_edit'},
            {'val': 'shrani in dodaj novo', 'name': 'save_and_add_another'},
            {'val': 'shrani in dodaj podobno', 'name': 'save_and_add_similar'},
            {'val': 'prekliči', 'name': 'cancel'}
            ]
    if request.method == 'POST':
        if 'cancel' in request.POST:
            return HttpResponseRedirect(reverse('struktura:kategorija', args=[key]))
        form = KategorijaForm(data=request.POST)
        if form.is_valid():
            kategorija = form.save(commit=False)
            kategorija.tekmovanje = tekmovanje
            kategorija.save()
            if 'save_and_exit' in request.POST:
                return HttpResponseRedirect(
                        reverse('struktura:kategorija', args=[key]))
            if 'save_and_edit' in request.POST:
                return HttpResponseRedirect(
                        reverse('struktura:kategorija_edit', args=[key, kategorija.pk]))
            if 'save_and_add_another' in request.POST:
                return HttpResponseRedirect(
                        reverse('struktura:kategorija_add', args=[key]))
    else:
        form = KategorijaForm()
    context['form'] = form
    return render(request, 'struktura/add.html', context)


def v_kategorija_rm(request, key, obj_key):
    kategorija = get_object_or_404(Kategorija, pk=obj_key)
    if kategorija.tekmovanje.pk == key:
        try:
            kategorija.delete()
            return HttpResponse('Successfully deleted Kategorija [{}]!'.format(str(kategorija)), status=200)
        except deletion.RestrictedError:
            return HttpResponse('Kategorija [{}] not deleted because of restricted foreign keys!'.format(str(kategorija)), status=500)
    return HttpResponse('Kategorija does not match tekmovanje pk, deletion refused!', status=403)
