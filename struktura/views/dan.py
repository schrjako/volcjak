from django.db.models import deletion
from django.shortcuts import render, get_object_or_404
from django import forms
from django.http import Http404, HttpResponseRedirect, HttpResponse
from django.urls import reverse

from datetime import date, timedelta

from ..models import Tekmovanje, Dan


class DanForm(forms.ModelForm):

    datum = forms.DateField(widget=forms.TextInput(attrs={'pattern': '\d\d\d\d-\d\d?-\d\d?',
                                                          'placeholder': '2022-7-21'}))

    class Meta:
        model = Dan
        exclude = ['tekmovanje']
        localized_fields = ('datum',)


def default_context(key):
    return {'key': key, 'sec_name': 'dan', 'display_name': 'Dan',
            'plural_name': 'dnevi'}


def v_dan(request, key):
    context = default_context(key)
    context['tekmovanje'] = tekmovanje = get_object_or_404(Tekmovanje, pk=key)
    context['objs'] = tekmovanje.dan_set.all()
    return render(request, 'struktura/index.html', context)


def v_dan_edit(request, key, obj_key):
    context = default_context(key)
    context['tekmovanje'] = tekmovanje = get_object_or_404(Tekmovanje, pk=key)
    context['obj'] = dan = get_object_or_404(Dan, pk=obj_key)
    context['submit_buttons'] = [
            {'val': 'shrani', 'name': 'save'},
            {'val': 'shrani in zapri', 'name': 'save_and_exit'},
            {'val': 'prekliči', 'name': 'cancel'}
            ]
    if dan.tekmovanje != tekmovanje:
        raise Http404("Dan not of right tekmovanje!")
    if request.method == 'POST':
        if 'cancel' in request.POST:
            return HttpResponseRedirect(reverse('struktura:dan', args=[key]))
        form = DanForm(data=request.POST, instance=dan)
        if form.is_valid():
            form.save()
            if 'save_and_exit' in request.POST:
                return HttpResponseRedirect(reverse('struktura:dan', args=[key]))
    else:
        form = DanForm(instance=dan)
    context['form'] = form
    return render(request, 'struktura/edit.html', context)


def v_dan_add(request, key):
    context = default_context(key)
    context['tekmovanje'] = tekmovanje = get_object_or_404(Tekmovanje, pk=key)
    context['submit_buttons'] = [
            {'val': 'shrani in zapri', 'name': 'save_and_exit'},
            {'val': 'shrani in uredi', 'name': 'save_and_edit'},
            {'val': 'shrani in dodaj novo', 'name': 'save_and_add_another'},
            {'val': 'shrani in dodaj podobno', 'name': 'save_and_add_similar'},
            {'val': 'prekliči', 'name': 'cancel'}
            ]
    if request.method == 'POST':
        if 'cancel' in request.POST:
            return HttpResponseRedirect(reverse('struktura:dan', args=[key]))
        form = DanForm(data=request.POST)
        if form.is_valid():
            dan = form.save(commit=False)
            dan.tekmovanje = tekmovanje
            dan.save()
            if 'save_and_exit' in request.POST:
                return HttpResponseRedirect(
                        reverse('struktura:dan', args=[key]))
            if 'save_and_edit' in request.POST:
                return HttpResponseRedirect(
                        reverse('struktura:dan_edit', args=[key, dan.pk]))
            if 'save_and_add_another' in request.POST:
                return HttpResponseRedirect(
                        reverse('struktura:dan_add', args=[key]))
            if 'save_and_add_similar' in request.POST:
                data = request.POST.copy()
                data['st'] = int(data['st']) + 1
                data['datum'] = date.fromisoformat(data['datum']) + timedelta(1)
                form = DanForm(data=data)
    else:
        form = DanForm()
    context['form'] = form
    return render(request, 'struktura/add.html', context)


def v_dan_rm(request, key, obj_key):
    dan = get_object_or_404(Dan, pk=obj_key)
    if dan.tekmovanje.pk == key:
        try:
            dan.delete()
            return HttpResponse('Successfully deleted Dan [{}]!'.format(str(dan)), status=200)
        except deletion.RestrictedError:
            return HttpResponse('Dan [{}] not deleted because of restricted foreign keys!'.format(str(dan)), status=500)
    return HttpResponse('Dan does not match tekmovanje pk, deletion refused!', status=403)
