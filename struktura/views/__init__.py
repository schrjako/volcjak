from django.http import JsonResponse
from django.shortcuts import get_object_or_404

from ..models import Tekmovanje
from .tekmovanje import v_tekmovanje
from .kategorija import v_kategorija, v_kategorija_edit, \
                        v_kategorija_add, v_kategorija_rm
from .dan import v_dan, v_dan_edit, v_dan_add, v_dan_rm
from .kt import v_kt, v_kt_edit, v_kt_add, v_kt_rm
from .ekipa import v_ekipa, v_ekipa_edit, v_ekipa_add, v_ekipa_rm
from .proga import v_proga, v_proga_edit, v_proga_add, v_proga_rm
from .kontrola import v_kontrola, v_kontrola_edit, v_kontrola_add, v_kontrola_rm
from .naloga import v_naloga, v_naloga_edit, v_naloga_add, v_naloga_rm
from .rezultat import v_rezultat, v_rezultat_edit, v_rezultat_add, v_rezultat_rm


def rezultati(request, key):
    tekmovanje = get_object_or_404(Tekmovanje, pk=key)
    return JsonResponse(tekmovanje.rezultati)


def pregled_ekipe(request, key):
    tekmovanje = get_object_or_404(Tekmovanje, pk=key)
    return JsonResponse(tekmovanje.pregled_ekipe)
