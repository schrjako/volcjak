from django.db import models, transaction
from django.forms import ValidationError
from django.utils.translation import gettext_lazy as _

from datetime import timedelta


class Proga(models.Model):

    kategorija = models.ForeignKey('Kategorija', on_delete=models.CASCADE)
    start = models.ForeignKey('KT', on_delete=models.CASCADE,
                              related_name='start', verbose_name='KT start')
    cilj = models.ForeignKey('KT', on_delete=models.CASCADE,
                             related_name='cilj', verbose_name='KT cilj')
    casovnica = models.DurationField('časovnica')
    odbitek = models.IntegerField('št izgubljenih točk', default=2)
    cas = models.DurationField('na čas', default=timedelta(minutes=1))

    class Meta:
        ordering = ['kategorija', 'start__dan__st']
        verbose_name_plural = 'proge'

    def clean(self):
        if self.start.dan != self.cilj.dan:
            raise ValidationError(_('Proga not descended from single dan!'))
        if self.start.dan.tekmovanje != self.kategorija.tekmovanje:
            raise ValidationError(_('Proga not descended from single tekmovanje!'))
        if (self.kontrola_set.all().count() > 0) and \
           (self.start.dan != Proga.objects.get(pk=self.pk).start.dan):
            raise ValidationError(_('Cannot change dan - it would make descended kontrolas descended from more than one dan!'))
        if self.pk is None:
            if Proga.objects.filter(start__dan=self.dan,
                                    kategorija=self.kategorija).count() > 0:
                raise ValidationError(_('Proga with this combination od dan and kategorija already exists!'))
        else:
            if Proga.objects.filter(start__dan=self.dan,
                                    kategorija=self.kategorija).count() > 1:
                raise ValidationError(_('Another proga with this combination od dan and kategorija already exists!'))
        super().clean()

    def __str__(self):
        return f"{self.kategorija} proga {self.dan.st}"

    @staticmethod
    def form(kat_qs, kt_qs):
        from django import forms
        class ProgaForm(forms.ModelForm):
            kategorija = forms.ModelChoiceField(queryset=kat_qs)
            start = forms.ModelChoiceField(queryset=kt_qs)
            cilj = forms.ModelChoiceField(queryset=kt_qs)
            casovnica = forms.DurationField(widget=forms.TextInput(attrs={'pattern':'\d\d?:\d\d?(:\d\d?)?',
                                                                          'placeholder':'HH:MM:SS'}))

            class Meta:
                model = Proga
                exclude = []

            def save(self, *args, **kwargs):
                from . import Kontrola, NalProS, NalProC
                with transaction.atomic():
                    if self.instance.pk is None:
                        proga = super().save(*args, **kwargs)
                        kstart = Kontrola.objects.create(ime='START', kt=self.instance.start, proga=self.instance)
                        kcilj = Kontrola.objects.create(ime='CILJ', kt=self.instance.cilj, proga=self.instance)
                        ncilj = NalProC.objects.create(kontrola=kcilj, ime='', min_tocke=0, max_tocke=0, ponovitve=1)
                        nstart = NalProS.objects.create(kontrola=kstart, ime='casovnica', min_tocke=-10000, max_tocke=0, ponovitve=1, cilj=ncilj)
                    else:
                        old_proga = Proga.objects.get(pk=self.instance.pk)
                        kstart = old_proga.kontrola_set.filter(ime='START').first()
                        kstart.kt = self.instance.start
                        kstart.save()
                        kcilj = old_proga.kontrola_set.filter(ime='CILJ').first()
                        kcilj.kt = self.instance.cilj
                        kcilj.save()
                        proga = super().save(*args, **kwargs)
                return proga

        return ProgaForm

    @classmethod
    def make_proga(cls, obj):
        for pk, proga in obj['Proga'].items():
            obj['Proga'][pk] = cls.objects.create(
                    kategorija=obj['Kategorija'][proga['kategorija']],
                    start=obj['KT'][proga['start']],
                    cilj=obj['KT'][proga['cilj']],
                    casovnica=timedelta(seconds=proga['casovnica']),
                    odbitek=proga['odbitek'],
                    cas=timedelta(seconds=proga['odbitek']))
        if 'Kontrola' in obj:
            from . import Kontrola
            Kontrola.make_kontrola(obj)

    def get_proga(self, selection, obj):
        if selection['Kontrola']:
            obj['Kontrola'].update({kontrola.pk: kontrola.get_kontrola(selection, obj) for kontrola in self.kontrola_set.all()})
        return {'kategorija': str(self.kategorija.pk), 'start': str(self.start.pk),
                'cilj': str(self.cilj.pk), 'casovnica': self.casovnica.total_seconds(),
                'odbitek': self.odbitek, 'cas': self.cas.total_seconds()}

    @property
    def dan(self):
        return self.start.dan

    @property
    def tekmovanje(self):
        return self.kategorija.tekmovanje

    def calculate_casovnica(self, ekipa=None):
        from . import RezProS
        qs_rez = RezProS.objects.filter(naloga__kontrola__proga=self)
        if ekipa is not None:
            qs_rez = qs_rez.filter(ekipa=ekipa)
        for rez in qs_rez:
            rez.calculate_points()
            rez.save()

    @property
    def pregled_ekipe(self):
        from . import Ekipa
        rez = {'pk': self.pk, 'ime': self.kategorija.ime + " proga " +
               str(self.dan.st), 'rows': {},
               'cols': [
                        {'pk': 'stevilka', 'ime': 'številka', 'head': True},
                        {'pk': 'ime', 'ime': 'ime', 'head': True},
                        {'pk': 'rod', 'ime': 'rod', 'head': True},
                        {'pk': 'kontakt', 'ime': 'kontakt', 'head': True},
                        ]
               }
        for ekipa in Ekipa.objects.filter(kategorija=self.kategorija):
            rez['rows'][ekipa.pk] = ekipa.pregled_ekipe(self)
        for kontrola in self.kontrola_set.all():
            rez['cols'].append({'pk': kontrola.pk, 'ime': kontrola.kt.ime})
        return rez

    def remove_children(self):
        for kontrola in self.kontrola_set.all():
            kontrola.remove_children()
            kontrola.delete()
