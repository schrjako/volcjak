from django.db import models
from django.forms import ValidationError
from django.utils.translation import gettext_lazy as _


class KT(models.Model):

    dan = models.ForeignKey('Dan', on_delete=models.CASCADE)
    ime = models.CharField(max_length=50)
    lokacija = models.CharField(max_length=50, blank=True)
    kontrolorji = models.CharField(max_length=100, blank=True)
    kontakt = models.CharField(max_length=50, blank=True)
    vrstni_red = models.SmallIntegerField()
    cip1_id = models.CharField('id čipirne postaje 1', max_length=20, blank=True)
    cip2_id = models.CharField('id čipirne postaje 2', max_length=20, blank=True)

    class Meta:
        ordering = ['dan', 'vrstni_red', 'ime']
        verbose_name = 'kontrolna točka'
        verbose_name_plural = 'kontrolne točke'

    def clean(self):
        if (self.start.all().count() + self.cilj.all().count() > 0) and \
            (self.dan != KT.objects.get(pk=self.pk).dan):
            raise ValidationError(_('Cannot change dan - it would make descended progas descended from more than one dan!'))
        if self.ime[:2].lower() in ['st', 'kt', 'ci']:
            self.vrstni_red = {'st': 0, 'kt': 1, 'ci': 2}[self.ime[:2].lower()]
        else:
            self.vrstni_red = 1
        super().clean()

    def __str__(self):
        return f"{self.dan} {self.ime}"

    @classmethod
    def make_kt(cls, obj):
        for pk, kt in obj['KT'].items():
            obj['KT'][pk] = cls(dan=obj['Dan'][kt['dan']])
            for attr, val in kt.items():
                if attr == 'dan':
                    continue
                setattr(obj['KT'][pk], attr, val)
            obj['KT'][pk].save()

    @property
    def tekmovanje(self):
        return self.dan.tekmovanje

    def get_kt(self, selection):
        return {'dan': str(self.dan.pk), 'ime': self.ime,
                'lokacija': self.lokacija, 'kontrolorji': self.kontrolorji,
                'kontakt': self.kontakt, 'vrstni_red': self.vrstni_red,
                'cip1_id': self.cip1_id, 'cip2_id': self.cip2_id}
