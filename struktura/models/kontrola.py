from datetime import timedelta

from django.db import models
from django.contrib import admin
from django.utils.html import format_html
from django.urls import reverse
from django.forms import ValidationError
from django.utils.translation import gettext_lazy as _


class Kontrola(models.Model):

    ime = models.CharField(max_length=30, blank=True)
    kt = models.ForeignKey('KT', on_delete=models.RESTRICT)
    proga = models.ForeignKey('Proga', on_delete=models.CASCADE)
    casovnica = models.DurationField('časovnica', blank=True, null=True)

    class Meta:
        constraints = [
            models.UniqueConstraint(fields=['kt', 'proga'],
                                    name='unique_kontrola'),
        ]
        ordering = ['proga', 'kt']
        verbose_name_plural = 'kontrole'

    def clean(self):
        if self.proga.start.dan != self.kt.dan:
            raise ValidationError(_('Kontrola not descended from single dan!'))
        super().clean()

    def __str__(self):
        return f"{self.proga} {self.ime}"

    @admin.display
    def pregled_link(self):
        return format_html("<a href='{}'>pregled</a>",
                           reverse('struktura:rezultat', args=[self.kt.pk]),
                           )

    @classmethod
    def make_kontrola(cls, obj):
        for pk, kontrola in obj['Kontrola'].items():
            obj['Kontrola'][pk] = cls.objects.create(
                    ime=kontrola['ime'], kt=obj['KT'][kontrola['kt']],
                    proga=obj['Proga'][kontrola['proga']],
                    casovnica=None if kontrola['casovnica'] is None else timedelta(seconds=kontrola['casovnica']))
        if 'Naloga' in obj:
            from . import Naloga
            Naloga.make_naloga(obj)
            #   Sorting subclasses by name is good, because ciljs have to be added
            #   before starts and 'C' < 'S'.
            for nal_subclass in sorted(Naloga.__subclasses__(), key=lambda subclass: subclass.__name__):
                if nal_subclass.__name__ in obj:
                    nal_subclass.make_naloga(obj)

    def get_kontrola(self, selection, obj):
        if selection['Naloga']:
            for naloga in self.naloga_set.all():
                naloga = naloga.child_obj
                obj[type(naloga).__name__][naloga.pk] = naloga.get_naloga()
        return {'ime': self.ime, 'kt': str(self.kt.pk), 'proga': str(self.proga.pk),
                'casovnica': None if self.casovnica is None else self.casovnica.total_seconds()}

    @property
    def dan(self):
        return self.kt.dan

    @property
    def kategorija(self):
        return self.proga.kategorija

    @property
    def tekmovanje(self):
        return self.proga.kategorija.tekmovanje

    @property
    def st_obiskov(self):
        from . import Naloga, Dogodek
        naloga = list(Naloga.objects.filter(kontrola=self, tip='KT'))[:1]
        if naloga == []:
            return 0
        return Dogodek.objects.filter(naloga=naloga[0]).count()

    @property
    def max_tocke(self):
        """
        Returns the total number of points achievable on this kontrola except for NalKTs
        """
        from . import NalKT
        ret = 0
        for nal in self.naloga_set.all():
            if type(nal.child_obj)!=NalKT:
                ret += nal.max_tocke
        return ret

    def remove_children(self):
        self.naloga_set.all().delete()
