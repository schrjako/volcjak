from django.db import models
from django.contrib import admin
from django.utils.html import format_html
from django.urls import reverse
from django.utils import timezone


class Ekipa(models.Model):

    kategorija = models.ForeignKey('Kategorija', on_delete=models.CASCADE)
    stevilka = models.SmallIntegerField('številka')
    rod = models.CharField(max_length=100)
    ime = models.CharField(max_length=50)
    kontakt = models.CharField(max_length=50, blank=True)
    st_clanov = models.SmallIntegerField('število članov', default=5)
    v_konkurenci = models.BooleanField('v konkurenci', default=True)
    info = models.JSONField(null=True, blank=True)
    cip_id = models.CharField('id čipa', max_length=20, blank=True)

    class Meta:
        ordering = ['kategorija', 'stevilka']
        verbose_name_plural = 'ekipe'

    def __str__(self):
        return f"{self.kategorija} {self.stevilka} {self.ime}"

    @admin.display
    def pregled_link(self):
        return format_html("<a href='{}'>pregled</a>",
                           reverse('pregled:ekipa_index', args=[self.pk]),
                           )

    @classmethod
    def make_ekipa(cls, obj):
        for pk, ekipa in obj['Ekipa'].items():
            obj['Ekipa'][pk] = cls(kategorija=obj['Kategorija'][ekipa['kategorija']])
            for attr, val in ekipa.items():
                if attr == 'kategorija':
                    continue
                setattr(obj['Ekipa'][pk], attr, val)
            obj['Ekipa'][pk].save()
        if 'Rezultat' in obj:
            from . import Rezultat
            Rezultat.make_rezultat(obj)
            #   Sorting subclasses by name is good, because ciljs have to be added
            #   before starts and 'C' < 'S'.
            for rez_subclass in sorted(Rezultat.__subclasses__(), key=lambda subclass: subclass.__name__):
                if rez_subclass.__name__ in obj:
                    rez_subclass.make_rezultat(obj)

    def get_ekipa(self, selection, obj):
        if selection['Rezultat']:
            for rezultat in self.rezultat_set.all():
                rezultat = rezultat.child_obj
                obj[type(rezultat).__name__][rezultat.pk] = rezultat.get_rezultat()
        return {'kategorija': str(self.kategorija.pk), 'stevilka': self.stevilka,
                'rod': self.rod, 'ime': self.ime, 'kontakt': self.kontakt,
                'st_clanov': self.st_clanov, 'v_konkurenci': self.v_konkurenci,
                'info': self.info, 'cip_id': self.cip_id}

    @property
    def tekmovanje(self):
        return self.kategorija.tekmovanje

    @property
    def short_name(self):
        return "<" + str(self.stevilka) + "> [" + self.rod + "] " + self.ime

    @property
    def rezultati(self):
        from . import RezKT
        rez = {'pk': self.pk, 'ime': self.ime, 'obscur': not self.v_konkurenci,
               'values': {'stevilka': self.stevilka,
                          'ime': self.ime,
                          'rod': self.rod,
                          'skupaj': 0,
                          },
               'urls': {
                   },
               }
        for rezultat in self.rezultat_set.all():
            if rezultat.kontrola.pk not in rez['values']:
                rez['values'][rezultat.kontrola.pk] = 0
            if type(rezultat.child_obj) == RezKT:
                if 'KT-ji' not in rez['values']:
                    rez['values']['KT-ji'] = 0
                rez['values']['KT-ji'] += rezultat.tocke
            else:
                rez['values'][rezultat.kontrola.pk] += rezultat.tocke
                rez['urls'][rezultat.kontrola.pk] = reverse('struktura:rezultat_edit',
                                                            args=[rezultat.kontrola.kt.pk, self.pk])
            rez['values']['skupaj'] += rezultat.tocke
        return rez

    @property
    def st_prebranih_obvestil(self):
        return self.obvestilo_set.filter(visible=True).count()

    @property
    def st_neprebranih_obvestil(self):
        return self.kategorija.tekmovanje.st_vidnih_obvestil - \
               self.st_prebranih_obvestil

    def pregled_ekipe(self, proga):
        from . import Rezultat
        from datetime import datetime
        rez = {'pk': self.pk, 'ime': self.ime,
               'values': {
                   'stevilka': self.stevilka,
                   'ime': self.ime,
                   'rod': self.rod,
                   'kontakt': self.kontakt,
               }
        }
        for kontrola in proga.kontrola_set.all():
            if kontrola.naloga_set.exists():
                small = min(map(lambda rez: rez.cas_vnosa, Rezultat.objects.filter(naloga__kontrola=kontrola, ekipa=self)), default=datetime.max)
            else:
                small = ""
            if small != "" and small != datetime.max:
                rez['values'][kontrola.pk] = small.astimezone(timezone.get_current_timezone()).strftime("%H:%M")
        return rez

    def remove_children(self):
        self.rezultat_set.all().delete()
