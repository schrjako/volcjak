from django.db import models
from django import forms
from django.forms import widgets, ValidationError
from django.utils.translation import gettext_lazy as _

from datetime import timedelta, datetime
from django.utils import timezone


class Rezultat(models.Model):

    naloga = models.ForeignKey('Naloga', on_delete=models.RESTRICT)
    ekipa = models.ForeignKey('Ekipa', on_delete=models.RESTRICT)
    mrtvi_cas = models.DurationField('mrtvi čas', default=timedelta(0, 0))
    tocke = models.FloatField('točke', blank=True)
    cas = models.DateTimeField('čas', default=timezone.now)
    cas_vnosa = models.DateTimeField('čas vnosa', auto_now_add=True)
    zadnji_popravek = models.DateTimeField('zadnji popravek', auto_now=True)

    class Meta:
        ordering = ['ekipa', 'naloga']
        verbose_name_plural = 'rezultati'

    def clean(self):
        if self.naloga.kontrola.proga.kategorija != self.ekipa.kategorija:
            raise forms.ValidationError(_('Rezultat not descended from single kategorija!'))
        super().clean()

    def __str__(self):
        return f"{self.ekipa} {self.naloga.ime}: {self.tocke}"

    @property
    def child_obj(self):
        for rez_class in Rezultat.__subclasses__():
            if hasattr(self, rez_class.__name__.lower()):
                return getattr(self, rez_class.__name__.lower())
        return self

    @staticmethod
    def form_field(naloga):
        return {'intype': forms.BooleanField(label=naloga.ime, required=False)}

    @staticmethod
    def nal_class():
        from . import Naloga
        return Naloga

    @classmethod
    def make_rezultat(cls, obj):
        name = cls.__name__
        for pk, rezultat in obj[name].items():
            obj[name][pk] = cls.objects.create(
                    naloga=obj[cls.nal_class().__name__][rezultat['naloga']],
                    ekipa=obj['Ekipa'][rezultat['ekipa']],
                    mrtvi_cas=timedelta(seconds=rezultat['mrtvi_cas']),
                    tocke=rezultat['tocke'],
                    cas=datetime.fromisoformat(rezultat['cas']))

    def get_rezultat(self):
        return {'naloga': str(self.naloga.pk), 'ekipa': str(self.ekipa.pk),
                'mrtvi_cas': self.mrtvi_cas.total_seconds(), 'tocke': self.tocke, 'cas': self.cas.isoformat()}

    @property
    def kontrola(self):
        return self.naloga.kontrola

    @property
    def proga(self):
        return self.naloga.kontrola.proga

    @property
    def kategorija(self):
        return self.ekipa.kategorijaj

    @property
    def kt(self):
        return self.naloga.kontrola.kt

    @property
    def dan(self):
        return self.naloga.kontrola.kt.dan

    @property
    def tekmovanje(self):
        return self.ekipa.kategorija.tekmovanje

    def update(self, mrtvi_cas, cas, form):
        self.mrtvi_cas = mrtvi_cas
        self.cas = cas
        self.tocke = self.naloga.child_obj.tocke_from_form(form)
        self.save()

    #   subtype specific, override if not the same for other Rez models
    def to_initial(self, naloga):
        return {'key': self.pk, 'infield': self.tocke == naloga.max_tocke}


class RezKT(Rezultat):

    class Meta:
        verbose_name = 'KT rezultat'
        verbose_name_plural = 'KT rezultati'

    @staticmethod
    def nal_class():
        from . import NalKT
        return NalKT

    @staticmethod
    def form_field(naloga):
        return {'intype': forms.BooleanField(widget=forms.HiddenInput, disabled=True, initial=True)}


class RezNum(Rezultat):

    @staticmethod
    def nal_class():
        from . import NalNum
        return NalNum

    @staticmethod
    def form_field(naloga):
        w = widgets.NumberInput(attrs={'min': naloga.min_tocke,
                                       'max': naloga.max_tocke,
                                       'step': naloga.korak})
        return {'intype': forms.FloatField(label=naloga.ime,
                                           initial=f"{naloga.min_tocke:g}",
                                           min_value=naloga.min_tocke,
                                           max_value=naloga.max_tocke,
                                           widget=w)}

    def to_initial(self, naloga):
        return {'key': self.pk, 'infield': f"{self.tocke:g}"}


class RezHitS(Rezultat):

    cilj = models.OneToOneField('RezHitC', on_delete=models.SET_NULL, related_name='start', null=True)

    class Meta:
        verbose_name = 'rezultat hitrostne etape starta'
        verbose_name_plural = 'rezultati hitrostne etape starta'

    @staticmethod
    def nal_class():
        from . import NalHitS
        return NalHitS

    @staticmethod
    def form_field(naloga):
        return {'intype': forms.SplitDateTimeField(label='Čas odhoda', initial=timezone.now)}

    @staticmethod
    def cilj_class():
        return RezHitC

    @property
    def timedelta(self):
        if self.cilj is None:
            return timedelta.max()
        return self.cilj.cas - self.cas

    @classmethod
    def make_rezultat(cls, obj):
        name = cls.__name__
        for pk, rezultat in obj[name].items():
            obj[name][pk] = cls.objects.create(
                    naloga=obj[cls.nal_class().__name__][rezultat['naloga']],
                    ekipa=obj['Ekipa'][rezultat['ekipa']],
                    mrtvi_cas=timedelta(seconds=rezultat['mrtvi_cas']),
                    tocke=rezultat['tocke'],
                    cas=datetime.fromisoformat(rezultat['cas']))
            if 'cilj' in rezultat:
                obj[name][pk].cilj = obj[cls.cilj_class().__name__][rezultat['cilj']]
                obj[name][pk].save()

    def get_rezultat(self):
        rezultat = super().get_rezultat()
        if self.cilj:
            rezultat['cilj'] = str(self.cilj.pk)
        return rezultat

    def to_initial(self, naloga):
        return {'key': self.pk, 'infield': self.cas}

    def update(self, mrtvi_cas, cas, form):
        self.mrtvi_cas = mrtvi_cas
        self.cas = form.cleaned_data['infield']
        self.save()


class RezHitC(Rezultat):

    class Meta:
        verbose_name = 'rezultat hitrostne etape cilja'
        verbose_name_plural = 'rezultati hitrostne etape cilja'

    def clean(self):
        if self.start.naloga != self.naloga.start:
            raise ValidationError(_("RezHitC's start's naloga must be its naloga's start!"))
        super().clean()

    @staticmethod
    def nal_class():
        from . import NalHitC
        return NalHitC

    @staticmethod
    def form_field(naloga):
        return {'intype': forms.SplitDateTimeField(label='Čas prihoda', initial=timezone.now)}

    def to_initial(self, naloga):
        return {'key': self.pk, 'infield': self.cas}

    def update(self, mrtvi_cas, cas, form):
        self.mrtvi_cas = mrtvi_cas
        self.cas = form.cleaned_data['infield']
        self.save()


class RezProS(Rezultat):

    cilj = models.OneToOneField('RezProC', on_delete=models.SET_NULL, related_name='start', null=True)

    class Meta:
        verbose_name = 'rezultat proge starta'
        verbose_name_plural = 'rezultati proge starta'

    @staticmethod
    def nal_class():
        from . import NalProS
        return NalProS

    @staticmethod
    def form_field(naloga):
        return {}

    @staticmethod
    def cilj_class():
        return RezProC

    @property
    def timedelta(self):
        if self.cilj is None:
            return timedelta.max()
        return timezone.make_naive(self.cilj.cas) - timezone.make_naive(self.cas)

    @classmethod
    def make_rezultat(cls, obj):
        name = cls.__name__
        for pk, rezultat in obj[name].items():
            obj[name][pk] = cls.objects.create(
                    naloga=obj[cls.nal_class().__name__][rezultat['naloga']],
                    ekipa=obj['Ekipa'][rezultat['ekipa']],
                    mrtvi_cas=timedelta(seconds=rezultat['mrtvi_cas']),
                    tocke=rezultat['tocke'],
                    cas=datetime.fromisoformat(rezultat['cas']))
            if 'cilj' in rezultat:
                obj[name][pk].cilj = obj[cls.cilj_class().__name__][rezultat['cilj']]
                obj[name][pk].save()

    def get_rezultat(self):
        rezultat = super().get_rezultat()
        if self.cilj:
            rezultat['cilj'] = str(self.cilj.pk)
        return rezultat

    def to_initial(self, naloga):
        return {'key': self.pk}

    def calculate_points(self):
        if self.cilj is not None:
            proga = self.proga
            qs_rez = Rezultat.objects.filter(naloga__kontrola__proga=proga, ekipa=self.ekipa).values('naloga__kontrola', 'mrtvi_cas')
            mrtvi_cas = timedelta(0)
            for curr_mrtvi_cas in {rez['naloga__kontrola']: rez['mrtvi_cas'] for rez in qs_rez}.values():
                mrtvi_cas += curr_mrtvi_cas
            self.tocke = round(min(mrtvi_cas + proga.casovnica - self.timedelta, timedelta(0)) / proga.cas * proga.odbitek)
        else:
            self.tocke = self.naloga.max_tocke

    def update(self, mrtvi_cas, cas, form):
        self.mrtvi_cas = mrtvi_cas
        self.cas = cas
        self.calculate_points()
        self.save()


class RezProC(Rezultat):

    class Meta:
        verbose_name = 'rezultat proge cilja'
        verbose_name_plural = 'rezultati proge cilja'

    def clean(self):
        if self.start.naloga != self.naloga.start:
            raise ValidationError(_("RezProC's start's naloga must be its naloga's start!"))
        super().clean()

    @staticmethod
    def nal_class():
        from . import NalProC
        return NalProC

    @staticmethod
    def form_field(naloga):
        return {}

    def to_initial(self, naloga):
        return {'key': self.pk}

    def update(self, mrtvi_cas, cas, form):
        self.mrtvi_cas = mrtvi_cas
        self.cas = cas
        if self.start is not None:
            self.start.calculate_points()
            self.start.save()
        self.save()

class RezCas(Rezultat):

    cas_izvajanja = models.DurationField('čas izvajanja', default=timedelta(0, 0))

    class Meta:
        verbose_name = 'rezultat cas'
        verbose_name_plural = 'rezultati cas'

    @staticmethod
    def form_field(naloga):
        return {'intype': forms.DurationField(label=naloga.ime)}

    @staticmethod
    def nal_class():
        from . import NalCas
        return NalCas

    @classmethod
    def make_rezultat(cls, obj):
        name = cls.__name__
        for pk, rezultat in obj[name].items():
            obj[name][pk] = cls.objects.create(
                    naloga=obj[cls.nal_class().__name__][rezultat['naloga']],
                    ekipa=obj['Ekipa'][rezultat['ekipa']],
                    mrtvi_cas=timedelta(seconds=rezultat['mrtvi_cas']),
                    tocke=rezultat['tocke'],
                    cas=datetime.fromisoformat(rezultat['cas']),
                    cas_izvajanja=timedelta(seconds=rezultat['cas_izvajanja']))

    def get_rezultat(self):
        rezultat = super().get_rezultat()
        rezultat['cas_izvajanja'] = self.cas_izvajanja.total_seconds()
        return rezultat

    def to_initial(self, naloga):
        return {'key': self.pk, 'infield': self.cas_izvajanja}

    def update(self, mrtvi_cas, cas, form):
        self.mrtvi_cas = mrtvi_cas
        self.cas = cas
        self.cas_izvajanja = form.cleaned_data['infield']
        self.save()
