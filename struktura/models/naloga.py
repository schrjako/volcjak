from django.db import models, transaction
from django.core import validators
from django import forms
from django.forms import ValidationError
from django.utils.translation import gettext_lazy as _

from datetime import timedelta


class Naloga(models.Model):

    eps = 1e-6

    kontrola = models.ForeignKey('Kontrola', on_delete=models.CASCADE)
    ime = models.CharField(max_length=50, blank=True)
    min_tocke = models.FloatField('najm. število točk', default=0)
    max_tocke = models.FloatField('najv. število točk', default=100)
    ponovitve = models.SmallIntegerField('število ponovitev', default=1)

    class Meta:
        constraints = [
                models.CheckConstraint(check=models.Q(ponovitve__gte=0),
                                       name='%(app_label)s_%(class)s_ponovitve_nonnegative')
                ]
        ordering = ['kontrola', 'ime']
        verbose_name_plural = 'naloge'

    def clean(self):
        if self.max_tocke < self.min_tocke:
            raise ValidationError(_('Max točke should be greater than min točke!'))
        super().clean()

    def __str__(self):
        return f"{type(self.child_obj).__name__}: {self.kontrola} {self.ime}"

    @staticmethod
    def has_form():
        return True

    @staticmethod
    def rez_class():
        from . import Rezultat
        return Rezultat

    @staticmethod
    def nal_type_to_obj():
        return {objtype.__name__: objtype for objtype in
                [Naloga] + Naloga.__subclasses__()}

    @classmethod
    def form(cls, qs):
        class NalogaForm(forms.ModelForm):
            kontrola = forms.ModelChoiceField(queryset=qs)

            class Meta:
                model = cls
                exclude = []

        return NalogaForm

    @classmethod
    def make_naloga(cls, obj):
        name = cls.__name__
        for pk, naloga in obj[name].items():
            obj[name][pk] = cls.objects.create(
                    kontrola=obj['Kontrola'][naloga['kontrola']],
                    ime=naloga['ime'], min_tocke=naloga['min_tocke'],
                    max_tocke=naloga['max_tocke'], ponovitve=naloga['ponovitve'])

    def get_naloga(self):
        return {'kontrola': str(self.kontrola.pk), 'ime': self.ime,
                'min_tocke': self.min_tocke, 'max_tocke': self.max_tocke,
                'ponovitve': self.ponovitve}

    def rez_form(self, needs_key):
        class RezultatForm(forms.Form):
            pass
        if needs_key:

            class RezultatForm(RezultatForm):
                key = forms.IntegerField(widget=forms.HiddenInput, disabled=True)

                def save(self, mrtvi_cas, cas):
                    from . import Rezultat
                    rezultat = Rezultat.objects.get(pk=self.cleaned_data['key'])
                    rezultat.child_obj.update(mrtvi_cas, cas, self)
        fields = self.rez_class().form_field(self)
        if 'intype' in fields:
            class RezultatForm(RezultatForm):
                infield = fields['intype']
        return RezultatForm

    def tocke_from_form(self, form):
        return self.max_tocke if 'infield' in form.cleaned_data and form.cleaned_data['infield'] else self.min_tocke

    def add_rezultats(self, ekipa, mrtvi_cas, cas, formset):
        with transaction.atomic():
            for form in formset:
                rezultat = self.rez_class()(naloga=self, ekipa=ekipa,
                                            mrtvi_cas=mrtvi_cas, cas=cas,
                                            tocke=self.tocke_from_form(form))
                rezultat.save()

    @property
    def base_rez_formset(self):
        class BaseRezFormSet(forms.BaseFormSet):

            def save(self, mrtvi_cas, cas):
                for form in self:
                    form.save(mrtvi_cas, cas)
        return BaseRezFormSet

    def rez_formset(self, data=None, initial=None):
        return forms.formset_factory(
                self.rez_form(initial is not None),
                formset=self.base_rez_formset,
                extra=self.ponovitve,
                max_num=self.ponovitve,
                validate_max=True)(data, initial=initial, prefix=self.pk)

    @property
    def child_obj(self):
        for nal_class in Naloga.__subclasses__():
            if hasattr(self, nal_class.__name__.lower()):
                return getattr(self, nal_class.__name__.lower())
        return self

    @property
    def kt(self):
        return self.kontrola.kt

    @property
    def dan(self):
        return self.kontrola.kt.dan

    @property
    def proga(self):
        return self.kontrola.proga

    @property
    def kategorija(self):
        return self.kontrola.proga.kategorija

    @property
    def tekmovanje(self):
        return self.kontrola.proga.kategorija.tekmovanje

    @property
    def delta_tocke(self):
        return self.max_tocke - self.min_tocke

    @property
    def children(self):
        return eval("self." + self.ime.lower().replace('_', '') + "_set.all()")

    @property
    def st_ekip(self):
        from . import Ekipa
        return Ekipa.objects.filter(kategorija=self.kategorija).count()

    @property
    def st_otrok(self):
        return self.children.count()


class NalKT(Naloga):

    class Meta:
        verbose_name = 'naloga KT'
        verbose_name_plural = 'naloge KT'

    @staticmethod
    def rez_class():
        from . import RezKT
        return RezKT

    @classmethod
    def form(cls, qs):
        class NalKTForm(forms.ModelForm):
            kontrola = forms.ModelChoiceField(queryset=qs)
            ime = forms.CharField(initial='KT', disabled=True)
            ponovitve = forms.CharField(initial=1, disabled=True)

            class Meta:
                model = cls
                exclude = []

        return NalKTForm

    def tocke_from_form(self, form):
        #   Needs to be directly set, because form.cleaned_data does not contain
        #   BooleanFields even with initial=True when they are disabled.
        return self.max_tocke


class NalNum(Naloga):

    korak = models.FloatField('korak', default=1, validators=[validators.MinValueValidator(Naloga.eps,
        message=_('Korak should be a positive floating point number (greater than eps = {})!'.format(Naloga.eps)))])

    def clean(self):
        if self.korak >= Naloga.eps and abs((self.delta_tocke / self.korak) - round(self.delta_tocke / self.korak)) > Naloga.eps:
            raise ValidationError(_('Delta tocke (max tocke - min tocke = {}) should be at least eps = {} close to a multiple of korak = {}!'.format(self.delta_tocke, Naloga.eps, self.korak)))
        super().clean()

    @staticmethod
    def rez_class():
        from . import RezNum
        return RezNum

    @classmethod
    def form(cls, qs):
        class NalNumForm(forms.ModelForm):
            kontrola = forms.ModelChoiceField(queryset=qs)

            class Meta:
                model = cls
                fields = ['kontrola', 'ime', 'min_tocke', 'max_tocke', 'korak', 'ponovitve']

        return NalNumForm

    @classmethod
    def make_naloga(cls, obj):
        name = cls.__name__
        for pk, naloga in obj[name].items():
            obj[name][pk] = cls.objects.create(
                    kontrola=obj['Kontrola'][str(naloga['kontrola'])], ime=naloga['ime'],
                    min_tocke=naloga['min_tocke'], max_tocke=naloga['max_tocke'],
                    ponovitve=naloga['ponovitve'], korak=naloga['korak'])



    def get_naloga(self):
        naloga = super().get_naloga()
        naloga['korak'] = self.korak
        return naloga

    def tocke_from_form(self, form):
        return form.cleaned_data['infield']


class NalHitS(Naloga):
    """
    Nalga describing start of hitrostna etapa.
    """

    cilj = models.OneToOneField('NalHitC', on_delete=models.CASCADE, related_name='start', null=True, blank=True)

    class Meta:
        verbose_name = 'naloga hitrostna start'
        verbose_name_plural = 'naloge hitrostna start'

    def clean(self):
        if self.cilj and self.kontrola.proga != self.cilj.kontrola.proga:
            raise ValidationError(_('Hitrostna etapa has to start and end on the same proga!'))
        #   It could be useful to allow that (for naloga where you have to go get sth).
        # if self.cilj and self.kontrola == self.cilj.kontrola:
        #     raise ValidationError(_('Hitrostna etapa cannot start and end on the same kontrola!'))
        super().clean()

    def delete(self):
        super().delete()
        if self.cilj is not None:
            self.cilj.delete()

    @staticmethod
    def rez_class():
        from . import RezHitS
        return RezHitS

    @staticmethod
    def cilj_class():
        return NalHitC

    @classmethod
    def form(cls, qs):
        class NalHitForm(forms.ModelForm):
            kontrola = forms.ModelChoiceField(queryset=qs, label='Start')
            kontrola_cilj = forms.ModelChoiceField(queryset=qs, label='Cilj')

            class Meta:
                model = cls
                fields = ['kontrola', 'kontrola_cilj', 'ime', 'min_tocke',
                          'max_tocke', 'ponovitve']

            def __init__(self, *args, **kwargs):
                super().__init__(*args, **kwargs)
                if 'instance' in kwargs:
                    self.fields['kontrola_cilj'].initial = kwargs['instance'].cilj.kontrola

            def clean(self):
                if self.cleaned_data['kontrola'].proga != self.cleaned_data['kontrola_cilj'].proga:
                    raise ValidationError(_('Hitrostna etapa has to start and end on the same proga!'))
                super().clean()

            def save(self):
                from django.db import transaction
                with transaction.atomic():
                    if self.instance.pk is not None:
                        nal_s = super().save()
                        nal_c = self.instance.cilj
                        for attr in self.changed_data:
                            if attr == 'kontrola':
                                continue
                            if attr == 'kontrola_cilj':
                                nal_c.kontrola = self.cleaned_data[attr]
                            else:
                                setattr(nal_c, attr, self.cleaned_data[attr])
                        nal_c.save()
                    else:
                        nal_s = super().save()
                        cd = self.cleaned_data
                        nal_c = NalHitC.objects.create(kontrola=cd['kontrola_cilj'], ime=cd['ime'], min_tocke=cd['min_tocke'],
                                                       max_tocke=cd['max_tocke'], ponovitve=cd['ponovitve'])
                        nal_s.cilj = nal_c
                        nal_s.save()
                return nal_s

        return NalHitForm

    @classmethod
    def make_naloga(cls, obj):
        name = cls.__name__
        for pk, naloga in obj[name].items():
            obj[name][pk] = cls.objects.create(
                    kontrola=obj['Kontrola'][str(naloga['kontrola'])], ime=naloga['ime'],
                    min_tocke=naloga['min_tocke'], max_tocke=naloga['max_tocke'],
                    ponovitve=naloga['ponovitve'])
            if naloga['cilj'] is not None:
                obj[name][pk].cilj = obj[cls.cilj_class().__name__][naloga['cilj']]
                obj[name][pk].save()

    def get_naloga(self):
        naloga = super().get_naloga()
        naloga['cilj'] = str(self.cilj.pk)
        return naloga

    def add_rezultats(self, ekipa, mrtvi_cas, cas, formset):
        with transaction.atomic():
            if self.cilj.rezultat_set.filter(ekipa=ekipa).exists():
                ciljs = list(map(lambda rez: rez.rezhitc, self.cilj.rezultat_set.filter(ekipa=ekipa).order_by('pk')))
                for i, form in enumerate(formset):
                    rezultat = self.rez_class()(naloga=self, ekipa=ekipa,
                            mrtvi_cas=mrtvi_cas, cas=form.cleaned_data['infield'],
                            tocke=self.min_tocke, cilj=ciljs[i])
                    rezultat.save()
                self.calculate_points()
            else:
                for i, form in enumerate(formset):
                    rezultat = self.rez_class()(naloga=self, ekipa=ekipa,
                            mrtvi_cas=mrtvi_cas, cas=form.cleaned_data['infield'], tocke=self.min_tocke)
                    rezultat.save()

    def calculate_points(self):
        if self.cilj is None:
            return
        rez_start_qs = self.rez_class().objects.filter(naloga=self).exclude(cilj__exact=None)
        if not rez_start_qs.exists():
            return
        best_time = min(map(lambda rez: rez.timedelta, rez_start_qs), default=timedelta.max)
        with transaction.atomic():
            for rez in rez_start_qs:
                rez.tocke = round(max(0, 2-rez.timedelta/best_time) * (self.max_tocke - self.min_tocke) + self.min_tocke)
                rez.save()

    @property
    def base_rez_formset(self):
        class BaseRezFormSet(forms.BaseFormSet):
            naloga = self

            def save(self, mrtvi_cas, cas):
                for form in self:
                    form.save(mrtvi_cas, cas)
                self.naloga.calculate_points()
        return BaseRezFormSet


class NalHitC(Naloga):
    """
    Naloga describing cilj of hitrostna etapa.
    """

    class Meta:
        verbose_name = 'naloga hitrostna cilj'
        verbose_name_plural = 'naloge hitrostna cilj'

    def clean(self):
        if self.start and self.kontrola.proga != self.start.kontrola.proga:
            raise ValidationError(_('Hitrostna etapa has to start and end on the same proga!'))
        #   It could be useful to disallow this.
        # if self.start and self.kontrola == self.start.kontrola:
        #     raise ValidationError(_('Hitrostna etapa cannot start and end on the same kontrola!'))
        super().clean()

    @staticmethod
    def has_form():
        return False

    @staticmethod
    def rez_class():
        from . import RezHitC
        return RezHitC

    @classmethod
    def form(cls, qs):
        NalHitForm = NalHitS.form(qs)

        class NalHitForm(NalHitForm):

            def __init__(self, instance=None, *args, **kwargs):
                if instance is not None:
                    super().__init__(instance=instance.start, *args, **kwargs)
                else:
                    super().__init__(*args, **kwargs)

        return NalHitForm

    def add_rezultats(self, ekipa, mrtvi_cas, cas, formset):
        with transaction.atomic():
            if self.start.rezultat_set.filter(ekipa=ekipa).exists():
                starts = list(map(lambda rez: rez.rezhits, self.start.rezultat_set.filter(ekipa=ekipa).order_by('pk')))
                for i, form in enumerate(formset):
                    rezultat = self.rez_class()(naloga=self, ekipa=ekipa,
                            mrtvi_cas=mrtvi_cas, cas=form.cleaned_data['infield'], tocke=self.min_tocke)
                    rezultat.save()
                    starts[i].cilj = rezultat
                    starts[i].save()
                self.start.calculate_points()
            else:
                for i, form in enumerate(formset):
                    rezultat = self.rez_class()(naloga=self, ekipa=ekipa,
                            mrtvi_cas=mrtvi_cas, cas=form.cleaned_data['infield'], tocke=self.min_tocke)
                    rezultat.save()

    @property
    def base_rez_formset(self):
        class BaseRezFormSet(forms.BaseFormSet):
            naloga = self

            def save(self, mrtvi_cas, cas):
                for form in self:
                    form.save(mrtvi_cas, cas)
                self.naloga.start.calculate_points()
        return BaseRezFormSet


class NalProS(Naloga):
    """
    Naloga describing start of proga.
    """

    cilj = models.OneToOneField('NalProC', on_delete=models.CASCADE, related_name='start', null=True, blank=True)

    class Meta:
        verbose_name = 'naloga proga start'
        verbose_name_plural = 'naloge proga start'

    def clean(self):
        if hasattr(self, 'cilj') and self.kontrola.proga != self.cilj.kontrola.proga:
            raise ValidationError(_('Proga has to start and end on the same proga!'))
        #   It could be useful to allow that (for naloga where you have to go get sth).
        # if self.cilj and self.kontrola == self.cilj.kontrola:
        #     raise ValidationError(_('Hitrostna etapa cannot start and end on the same kontrola!'))
        super().clean()

    def delete(self):
        super().delete()
        if self.cilj is not None:
            self.cilj.delete()

    @staticmethod
    def has_form():
        return False

    @staticmethod
    def rez_class():
        from . import RezProS
        return RezProS

    @staticmethod
    def cilj_class():
        return NalProC

    @classmethod
    def make_naloga(cls, obj):
        name = cls.__name__
        for pk, naloga in obj[name].items():
            obj[name][pk] = cls.objects.create(
                    kontrola=obj['Kontrola'][str(naloga['kontrola'])], ime=naloga['ime'],
                    min_tocke=naloga['min_tocke'], max_tocke=naloga['max_tocke'],
                    ponovitve=naloga['ponovitve'])
            if naloga['cilj'] is not None:
                obj[name][pk].cilj = obj[cls.cilj_class().__name__][naloga['cilj']]
                obj[name][pk].save()

    def get_naloga(self):
        naloga = super().get_naloga()
        naloga['cilj'] = str(self.cilj.pk)
        return naloga

    def add_rezultats(self, ekipa, mrtvi_cas, cas, formset):
        with transaction.atomic():
            if self.cilj.rezultat_set.filter(ekipa=ekipa).exists():
                ciljs = list(map(lambda rez: rez.rezproc, self.cilj.rezultat_set.filter(ekipa=ekipa).order_by('pk')))
                for i, form in enumerate(formset):
                    rezultat = self.rez_class()(naloga=self, ekipa=ekipa,
                            mrtvi_cas=mrtvi_cas, cas=cas,
                            tocke=self.max_tocke, cilj=ciljs[i])
                    rezultat.save()
            else:
                for i, form in enumerate(formset):
                    rezultat = self.rez_class()(naloga=self, ekipa=ekipa,
                            mrtvi_cas=mrtvi_cas, cas=cas, tocke=self.max_tocke)
                    rezultat.save()


class NalProC(Naloga):
    """
    Naloga describing cilj of proga.
    """

    class Meta:
        verbose_name = 'naloga proga cilj'
        verbose_name_plural = 'naloge proga cilj'

    def clean(self):
        if hasattr(self, 'start') and self.kontrola.proga != self.start.kontrola.proga:
            raise ValidationError(_('Proga has to start and end on the same proga!'))
        #   It could be useful to disallow this.
        # if self.start and self.kontrola == self.start.kontrola:
        #     raise ValidationError(_('Hitrostna etapa cannot start and end on the same kontrola!'))
        super().clean()

    @staticmethod
    def has_form():
        return False

    @staticmethod
    def rez_class():
        from . import RezProC
        return RezProC

    def add_rezultats(self, ekipa, mrtvi_cas, cas, formset):
        with transaction.atomic():
            if self.start.rezultat_set.filter(ekipa=ekipa).exists():
                starts = list(map(lambda rez: rez.rezpros, self.start.rezultat_set.filter(ekipa=ekipa).order_by('pk')))
                for i, form in enumerate(formset):
                    rezultat = self.rez_class()(naloga=self, ekipa=ekipa,
                            mrtvi_cas=mrtvi_cas, cas=cas, tocke=self.max_tocke)
                    rezultat.save()
                    starts[i].cilj = rezultat
                    starts[i].save()
            else:
                for i, form in enumerate(formset):
                    rezultat = self.rez_class()(naloga=self, ekipa=ekipa,
                            mrtvi_cas=mrtvi_cas, cas=cas, tocke=self.max_tocke)
                    rezultat.save()

class NalCas(Naloga):

    class Meta:
        verbose_name = 'naloga cas'
        verbose_name_plural = 'naloge cas'

    @staticmethod
    def rez_class():
        from . import RezCas
        return RezCas

    def add_rezultats(self, ekipa, mrtvi_cas, cas, formset):
        with transaction.atomic():
            for form in formset:
                rezultat = self.rez_class()(naloga=self, ekipa=ekipa,
                                            mrtvi_cas=mrtvi_cas, cas=cas,
                                            tocke=0, cas_izvajanja=form.
                                            cleaned_data['infield'])
                rezultat.save()
            self.calculate_points()

    def calculate_points(self):
        rez_qs = self.rez_class().objects.filter(naloga=self)
        if not rez_qs.exists():
            return
        best_time = min(map(lambda rez: rez.cas_izvajanja, rez_qs), default=timedelta.max)
        worst_time = max(map(lambda rez: rez.cas_izvajanja, rez_qs), default=timedelta(0))
        with transaction.atomic():
            for rez in rez_qs:
                if best_time == worst_time:
                    rez.tocke = self.max_tocke
                else:
                    rez.tocke = round((worst_time - rez.cas_izvajanja) / (worst_time - best_time) * (self.max_tocke - self.min_tocke) + self.min_tocke)
                rez.save()

    @property
    def base_rez_formset(self):
        class BaseRezFormSet(forms.BaseFormSet):
            naloga = self

            def save(self, mrtvi_cas, cas):
                for form in self:
                    form.save(mrtvi_cas, cas)
                self.naloga.calculate_points()
        return BaseRezFormSet
