from django.db import models, transaction
from django.contrib import admin
from django.utils.html import format_html
from django.urls import reverse


class Tekmovanje(models.Model):

    leto = models.SmallIntegerField()
    ime = models.CharField(max_length=100)

    class Meta:
        ordering = ['leto', 'ime']
        verbose_name_plural = 'tekmovanja'

    def __str__(self):
        return f"{self.ime} {self.leto}"

    @admin.display
    def pregled_link(self):
        return format_html("<a href='{}'>pregled</a>",
                           reverse('pregled:index', args=[self.pk]),
                           )

    @admin.display
    def struktura_link(self):
        return format_html("<a href='{}'>struktura</a>",
                           reverse('struktura:tekmovanje', args=[self.pk]),
                           )

    def get_tekmovanje(self, selection={'Dan': True, 'Kategorija': True,
                                        'KT': True, 'Proga': True,
                                        'kontrola': True, 'Ekipa': True,
                                        'Naloga': True, 'Rezultat': False}):
        from . import Naloga, Rezultat
        if not selection['Dan']:
            selection['KT'] = False
        if not selection['Kategorija']:
            selection['Proga'] = selection['Ekipa'] = False
        if not selection['KT']:
            selection['Proga'] = selection['Kontrola'] = False
        if not selection['Proga']:
            selection['Kontrola'] = False
        if not selection['Kontrola']:
            selection['Naloga'] = False
        if not selection['Naloga'] or not selection['Ekipa']:
            selection['Rezultat'] = False

        tekmovanje = {'leto': self.leto, 'ime': self.ime}
        obj = {name: {} for name in selection if selection[name]}
        if selection['Naloga']:
            obj.update({subclass.__name__: {} for subclass in Naloga.__subclasses__()})
        if selection['Rezultat']:
            obj.update({subclass.__name__: {} for subclass in Rezultat.__subclasses__()})

        obj['Tekmovanje'] = {self.pk: tekmovanje}

        if selection['Dan']:
            obj['Dan'] = {dan.pk: dan.get_dan(selection, obj) for dan in self.dan_set.all()}
        if selection['Kategorija']:
            obj['Kategorija'] = {kategorija.pk: kategorija.get_kategorija(selection, obj) for kategorija in self.kategorija_set.all()}
        return obj

    def make_tekmovanje(self, obj):
        from . import Dan, Kategorija
        with transaction.atomic():
            for pk in obj['Tekmovanje']:
                self.leto = obj['Tekmovanje'][pk]['leto']
                self.ime = obj['Tekmovanje'][pk]['ime']
                self.save()
                obj['Tekmovanje'][pk] = self
            if 'Dan' in obj:
                Dan.make_dan(obj)
            if 'Kategorija' in obj:
                Kategorija.make_kategorija(obj)

    @property
    def st_obvestil(self):
        return self.obvestilo_set.all().count()

    @property
    def st_vidnih_obvestil(self):
        return self.obvestilo_set.filter(visible=True).count()

    @property
    def st_nevidnih_obvestil(self):
        return self.obvestilo_set.filter(visible=False).count()

    @property
    def ekipe(self):
        from . import Ekipa
        return Ekipa.objects.filter(kategorija__tekmovanje=self)

    @property
    def st_ekip(self):
        return self.ekipe.count()

    @property
    def rezultati(self):
        rez = {'ime': self.ime, 'leto': self.leto, 'key': self.pk, 'tabs': {}}
        for kategorija in self.kategorija_set.all():
            rez['tabs'][kategorija.pk] = kategorija.rezultati
        return rez

    @property
    def pregled_ekipe(self):
        from . import Proga
        rez = {'ime': self.ime, 'leto': self.leto, 'key': self.pk, 'tabs': {}}
        for proga in Proga.objects.filter(kategorija__tekmovanje=self):
            rez['tabs'][proga.pk] = proga.pregled_ekipe
        return rez

    def remove_children(self):
        with transaction.atomic():
            for kategorija in self.kategorija_set.all():
                kategorija.remove_children()
                kategorija.delete()
            for dan in self.dan_set.all():
                dan.remove_children()
                dan.delete()
