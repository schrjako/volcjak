from django.db import models
from django.forms import ValidationError
from django.utils.translation import gettext_lazy as _

from packaging import version


class Kategorija(models.Model):

    tekmovanje = models.ForeignKey('Tekmovanje', on_delete=models.CASCADE)
    ime = models.CharField(max_length=50)

    class Meta:
        verbose_name_plural = 'kategorije'

    def clean(self):
        if (self.proga_set.all().count() > 0) and \
           (self.tekmovanje != Kategorija.objects.get(pk=self.pk).tekmovanje):
            raise ValidationError(_('Cannot change tekmovanje - it would make descended progas descended from more than one tekmovanje!'))
        super().clean()

    def __str__(self):
        return self.ime

    @classmethod
    def make_kategorija(cls, obj):
        for pk, kategorija in obj['Kategorija'].items():
            obj['Kategorija'][pk] = cls.objects.create(tekmovanje=obj['Tekmovanje'][kategorija['tekmovanje']], ime=kategorija['ime'])
        if 'Proga' in obj:
            from . import Proga
            Proga.make_proga(obj)
        if 'Ekipa' in obj:
            from . import Ekipa
            Ekipa.make_ekipa(obj)

    def get_kategorija(self, selection, obj):
        if selection['Ekipa']:
            obj['Ekipa'].update({ekipa.pk: ekipa.get_ekipa(selection, obj) for ekipa in self.ekipa_set.all()})
        if selection['Proga']:
            obj['Proga'].update({proga.pk: proga.get_proga(selection, obj) for proga in self.proga_set.all()})
        return {'tekmovanje': str(self.tekmovanje.pk), 'ime': self.ime}

    @property
    def st_ekip(self):
        return self.ekipa_set.all().count()

    @property
    def max_row(self):
        from . import Kontrola, NalKT
        bestvals = {kontrola.pk: kontrola.max_tocke for kontrola in Kontrola.objects.filter(proga__kategorija=self)}
        bestvals['KT-ji'] = sum(nal.max_tocke for nal in NalKT.objects.filter(kontrola__in=bestvals))
        bestskupaj = sum(bestvals.values())
        bestvals['skupaj'] = bestskupaj
        row = {'ime': 'MAX', 'obscur': True, 'values': {
               'ime': 'MAX',
               'mesto': '/',
               'rod': '/',
               'stevilka': '/' } | bestvals}
        return row

    @property
    def rezultati(self):
        from django.urls import reverse
        from . import Kontrola, NalKT, Naloga
        rez = {'pk': self.pk, 'ime': self.ime, 'rows': [],
               'cols': [
                        {'pk': 'mesto', 'ime': 'mesto', 'head': True},
                        {'pk': 'stevilka', 'ime': 'številka', 'head': True},
                        {'pk': 'ime', 'ime': 'ime', 'head': True},
                        {'pk': 'rod', 'ime': 'rod', 'head': True},
                        {'pk': 'skupaj', 'ime': 'skupaj'},
                       ]
               }
        for ekipa in self.ekipa_set.all():
            rez['rows'].append(ekipa.rezultati)
        rez['rows'].append(self.max_row)
        rez['rows'].sort(key=lambda row: -row['values']['skupaj'])
        if rez['rows']:
            i = 1
            oldi = 1
            psk = rez['rows'][0]['values']['skupaj'] - 1
            for row in rez['rows']:
                if psk != row['values']['skupaj']:
                    oldi = i
                row['values']['mesto'] = oldi
                psk = row['values']['skupaj']
                row['values']['skupaj'] = f"{str(row['values']['skupaj']):.6}"
                if not row['obscur']:
                    i += 1
            rez['rows'][0]['values']['mesto'] = '/'
        kontrolas = sorted(list(Kontrola.objects.filter(proga__kategorija=self).exclude(ime='CILJ')),
            key=lambda kontrola: (kontrola.kt.dan.st, kontrola.kt.vrstni_red, version.parse(kontrola.kt.ime)))
        if NalKT.objects.filter(kontrola__proga__kategorija=self).exists():
            rez['cols'].append({'pk': 'KT-ji', 'ime': 'KT'})
        for kontrola in kontrolas:
            if NalKT.objects.filter(kontrola=kontrola).count() == Naloga.objects.filter(kontrola=kontrola).count():
                continue
            rez['cols'].append({'pk': kontrola.pk, 'ime': kontrola.ime,
                                'url': reverse('struktura:rezultat', args=[kontrola.kt.pk]) +
                                        f"#tabs-{kontrola.kategorija.pk}"})
            if kontrola.ime == 'START':
                if self.proga_set.all().count() == 1:
                    rez['cols'][-1]['ime'] = "časovnica"
                else:
                    rez['cols'][-1]['ime'] = f"časovnica {kontrola.kt.dan.st}"
        return rez

    def remove_children(self):
        for ekipa in self.ekipa_set.all():
            ekipa.remove_children()
            ekipa.delete()
        for proga in self.proga_set.all():
            proga.remove_children()
            proga.delete()
