from struktura.models.tekmovanje import Tekmovanje
from struktura.models.dan import Dan
from struktura.models.kategorija import Kategorija
from struktura.models.kt import KT
from struktura.models.proga import Proga
from struktura.models.kontrola import Kontrola
from struktura.models.ekipa import Ekipa
from struktura.models.naloga import Naloga, NalKT, NalNum, NalHitS, NalHitC, NalProS, NalProC, NalCas
from struktura.models.rezultat import Rezultat, RezKT, RezNum, RezHitS, RezHitC, RezProS, RezProC, RezCas
