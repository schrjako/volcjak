from datetime import date

from django.db import models
from django.forms import ValidationError
from django.utils.translation import gettext_lazy as _


class Dan(models.Model):

    tekmovanje = models.ForeignKey('Tekmovanje', on_delete=models.CASCADE)
    st = models.SmallIntegerField('številka dneva')
    datum = models.DateField()

    class Meta:
        ordering = ['tekmovanje', 'datum']
        verbose_name_plural = 'dnevi'

    def clean(self):
        if (self.kt_set.all().count() > 0) and \
           (self.tekmovanje != Dan.objects.get(pk=self.pk).tekmovanje):
            raise ValidationError(_('Cannot change tekmovanje - it would make descended KTs descended from more than one tekmovanje!'))
        super().clean()

    def __str__(self):
        return f"Dan {self.st}"

    @classmethod
    def make_dan(cls, obj):
        for pk, dan in obj['Dan'].items():
            obj['Dan'][pk] = cls.objects.create(tekmovanje=obj['Tekmovanje'][dan['tekmovanje']],
                                     st=dan['st'], datum=date.fromordinal(dan['datum']))
        if 'KT' in obj:
            from . import KT
            KT.make_kt(obj)

    def get_dan(self, selection, obj):
        if selection['KT']:
            obj['KT'].update({kt.pk: kt.get_kt(selection) for kt in self.kt_set.all()})
        return {'tekmovanje': str(self.tekmovanje.pk), 'st': self.st, 'datum':
                self.datum.toordinal()}

    def remove_children(self):
        self.kt_set.all().delete()
