from django.apps import AppConfig


class StrukturaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'struktura'
