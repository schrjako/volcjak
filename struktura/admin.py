from django.contrib import admin

from .models import Tekmovanje, Kategorija, Dan, KT, Proga, Kontrola, Ekipa
from .models import Naloga, NalKT, NalNum, NalHitS, NalHitC, NalProS, NalProC, NalCas
from .models import Rezultat, RezKT, RezNum, RezHitS, RezHitC, RezProS, RezProC, RezCas


class TekmovanjeAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'pregled_link', 'struktura_link')


admin.site.register(Tekmovanje, TekmovanjeAdmin)

admin.site.register(Kategorija)
admin.site.register(Dan)


class KTAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'cip1_id', 'cip2_id')
    exclude = ('vrstni_red', )


admin.site.register(KT, KTAdmin)

admin.site.register(Proga)


class EkipaAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'pregled_link')


admin.site.register(Ekipa, EkipaAdmin)


class KontrolaAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'pregled_link')


admin.site.register(Kontrola, KontrolaAdmin)

admin.site.register(Naloga)
admin.site.register(NalKT)
admin.site.register(NalNum)
admin.site.register(NalHitS)
admin.site.register(NalHitC)
admin.site.register(NalProS)
admin.site.register(NalProC)
admin.site.register(NalCas)

admin.site.register(Rezultat)
admin.site.register(RezKT)
admin.site.register(RezNum)
admin.site.register(RezHitS)
admin.site.register(RezHitC)
admin.site.register(RezProS)
admin.site.register(RezProC)
admin.site.register(RezCas)
