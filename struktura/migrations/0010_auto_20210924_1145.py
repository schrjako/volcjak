# Generated by Django 3.2.7 on 2021-09-24 09:45

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('struktura', '0009_auto_20210924_0314'),
    ]

    operations = [
        migrations.AlterField(
            model_name='kt',
            name='kontakt',
            field=models.CharField(blank=True, max_length=50),
        ),
        migrations.AlterField(
            model_name='kt',
            name='kontrolorji',
            field=models.CharField(blank=True, max_length=100),
        ),
        migrations.AlterField(
            model_name='kt',
            name='lokacija',
            field=models.CharField(blank=True, max_length=50),
        ),
    ]
