# Generated by Django 4.0.5 on 2022-06-24 09:16

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('struktura', '0033_nalhitc_nalhits_nalkt_rezhitc_rezhits_rezkt_and_more'),
    ]

    operations = [
        migrations.CreateModel(
            name='NalProC',
            fields=[
                ('naloga_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='struktura.naloga')),
            ],
            bases=('struktura.naloga',),
        ),
        migrations.CreateModel(
            name='NalProS',
            fields=[
                ('naloga_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='struktura.naloga')),
                ('cilj', models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='start', to='struktura.nalproc')),
            ],
            bases=('struktura.naloga',),
        ),
        migrations.CreateModel(
            name='RezProC',
            fields=[
                ('rezultat_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='struktura.rezultat')),
            ],
            bases=('struktura.rezultat',),
        ),
        migrations.CreateModel(
            name='RezProS',
            fields=[
                ('rezultat_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='struktura.rezultat')),
                ('cilj', models.OneToOneField(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='start', to='struktura.rezproc')),
            ],
            bases=('struktura.rezultat',),
        ),
        migrations.AlterField(
            model_name='kontrola',
            name='casovnica',
            field=models.DurationField(blank=True, null=True, verbose_name='časovnica'),
        ),
        migrations.DeleteModel(
            name='NalCas',
        ),
    ]
