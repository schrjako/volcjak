# Generated by Django 3.2.7 on 2021-09-12 19:50

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('struktura', '0003_auto_20210912_1544'),
    ]

    operations = [
        migrations.CreateModel(
            name='Dan',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('st', models.SmallIntegerField(verbose_name='številka dneva')),
                ('datum', models.DateField()),
            ],
            options={
                'ordering': ['tekmovanje', 'datum'],
            },
        ),
        migrations.AlterModelOptions(
            name='dogodek',
            options={'ordering': ['ekipa', 'naloga']},
        ),
        migrations.AlterModelOptions(
            name='ekipa',
            options={'ordering': ['kategorija', 'stevilka']},
        ),
        migrations.AlterModelOptions(
            name='kontrola',
            options={'ordering': ['proga', 'KT']},
        ),
        migrations.AlterModelOptions(
            name='kt',
            options={'ordering': ['dan', 'ime']},
        ),
        migrations.AlterModelOptions(
            name='naloga',
            options={'ordering': ['kontrola', 'tip']},
        ),
        migrations.AlterModelOptions(
            name='proga',
            options={'ordering': ['kategorija', 'start__dan__st']},
        ),
        migrations.AlterModelOptions(
            name='rezultat',
            options={'ordering': ['ekipa', 'naloga']},
        ),
        migrations.AlterModelOptions(
            name='tekmovanje',
            options={'ordering': ['leto', 'ime']},
        ),
        migrations.RemoveField(
            model_name='kt',
            name='tekmovanje',
        ),
        migrations.RemoveField(
            model_name='proga',
            name='ime',
        ),
        migrations.AlterField(
            model_name='naloga',
            name='tip',
            field=models.CharField(choices=[('KT', 'KT'), ('naloga', 'naloga'), ('hstart', 'hitrostna start'), ('hcilj', 'hitrostna cilj'), ('pstart', 'proga start'), ('pcilj', 'proga cilj')], default='naloga', max_length=10),
        ),
        migrations.AddConstraint(
            model_name='dogodek',
            constraint=models.UniqueConstraint(fields=('naloga', 'ekipa'), name='unique_dogodek'),
        ),
        migrations.AddConstraint(
            model_name='kontrola',
            constraint=models.UniqueConstraint(fields=('KT', 'proga'), name='unique_kontrola'),
        ),
        migrations.AddConstraint(
            model_name='rezultat',
            constraint=models.UniqueConstraint(fields=('naloga', 'ekipa'), name='unique_rezultat'),
        ),
        migrations.AddField(
            model_name='dan',
            name='tekmovanje',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='struktura.tekmovanje'),
        ),
        migrations.RunSQL(
            "INSERT INTO struktura_dan (st, datum, tekmovanje_id) SELECT 1, '2021-9-24', (SELECT id FROM struktura_tekmovanje ORDER BY (id) DESC LIMIT 1) WHERE EXISTS (SELECT id FROM struktura_tekmovanje)",
            reverse_sql = "DELETE FROM struktura_dan",
        ),
        migrations.AddField(
            model_name='kt',
            name='dan',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='struktura.dan'),
            preserve_default=False,
        ),
    ]
