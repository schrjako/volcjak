# Generated by Django 3.2.7 on 2021-09-24 13:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('struktura', '0011_kontrola_ime'),
    ]

    operations = [
        migrations.AddField(
            model_name='ekipa',
            name='cip_id',
            field=models.CharField(blank=True, max_length=20, verbose_name='id čipa'),
        ),
    ]
