# Generated by Django 4.0.4 on 2022-05-07 12:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('struktura', '0026_reznum'),
    ]

    operations = [
        migrations.AddField(
            model_name='rezultat',
            name='cas',
            field=models.DateTimeField(default='2022-05-07 14:54:50+0200', verbose_name='čas'),
        ),
    ]
