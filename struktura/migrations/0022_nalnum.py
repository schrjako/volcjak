# Generated by Django 3.2.10 on 2022-04-03 20:09

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('struktura', '0021_naloga_ponovitve'),
    ]

    operations = [
        migrations.CreateModel(
            name='NalNum',
            fields=[
                ('naloga_ptr', models.OneToOneField(auto_created=True, on_delete=django.db.models.deletion.CASCADE, parent_link=True, primary_key=True, serialize=False, to='struktura.naloga')),
                ('korak', models.FloatField(default=1.0, verbose_name='korak')),
            ],
            bases=('struktura.naloga',),
        ),
    ]
