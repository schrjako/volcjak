# Generated by Django 4.0.4 on 2022-05-07 20:00

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('struktura', '0028_proga_cas_proga_odbitek_alter_rezultat_cas'),
    ]

    operations = [
        migrations.AlterField(
            model_name='proga',
            name='odbitek',
            field=models.IntegerField(default=2, verbose_name='št izgubljenih točk'),
        ),
    ]
