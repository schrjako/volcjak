from django.urls import path, register_converter
from django.conf import settings

from . import views
import converters

register_converter(converters.makeConverter(
    settings.HASHIDS_SALT['rezultat']), 'rhash')
register_converter(converters.makeConverter(
    settings.HASHIDS_SALT['struktura']), 'shash')
register_converter(converters.makeConverter(
    settings.HASHIDS_SALT['kontrola']), 'khash')

app_name = 'struktura'
urlpatterns = [
    path('tekmovanje/<shash:key>/', views.v_tekmovanje, name='tekmovanje'),
]

for modelname in ['kategorija', 'dan', 'kt', 'ekipa', 'proga', 'kontrola', 'naloga']:
    for op in ['', 'edit', 'add', 'rm']:
        cname = modelname
        if op != '':
            cname += '_' + op
        cpath = cname.replace('_', '/') + '/<shash:key>/'
        if op in ['edit', 'rm']:
            cpath += '<shash:obj_key>/'
        cview = getattr(views, 'v_' + cname),
        urlpatterns.append(path(cpath, *cview, name=cname))

urlpatterns.append(path('rezultat/<khash:key>', views.v_rezultat, name='rezultat'))
urlpatterns.append(path('rezultat/a/<khash:key>/<khash:kat_key>', views.v_rezultat_add, name='rezultat_add'))
urlpatterns.append(path('rezultat/e/<khash:key>/<khash:eki_key>', views.v_rezultat_edit, name='rezultat_edit'))
urlpatterns.append(path('rezultat/r/<khash:key>/<khash:eki_key>', views.v_rezultat_rm, name='rezultat_rm'))

urlpatterns.append(path('rezultati/<rhash:key>/',
                        views.rezultati, name='rezultati'))
urlpatterns.append(path('pregled_ekipe/<rhash:key>/',
                   views.pregled_ekipe, name='pregled_ekipe'))
