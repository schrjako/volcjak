from hashids import Hashids
from django.conf import settings
from struktura.models import Tekmovanje

def makeConverter(salt):

    hashids = Hashids(salt, min_length=10)

    class HashConverter:

        regex = '[0-9a-zA-Z]{10,}'

        def to_python(self, value):
            id = hashids.decode(value)
            if not id:
                raise ValueError("Hash doesn't resolve into an integer (which it should")
            #id = id[0]
            return id[0]

        def to_url(self, value):
            return hashids.encode(value)

    return HashConverter
