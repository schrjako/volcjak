# Volčjak

## Navodila za uporabo

Po kloniranju, migriranju, in konfiguraciji Volčjaka, ga lahko zaženeš.
*Tekmovanje* narediš skozi django admin vmesnik (za katerega rabiš prej
ustvarjen račun). Ko si tekmovanje dodal ti django admin pokaže dve povezavi v
tabu *Tekmovanja*.

Prva povezava (*pregled*) ti pokaže generalni pregled nad tekmovanjem. To je del, ki se ga
uporablja med izvedbo samega tekmovanja. Več informacij se nahaja v spodnjem
podpoglavju [Pregled](#pregled).

Druga povezava (*struktura*) odpre vmesnik, za nastavitev tekmovanja. To bi
lahko počeli tudi skoyi django admin vmesnik, vendar to postane veliko bolj
nepregledno, čim imaš v bazi več kot eno tekmovanje. Več informacij se nahaja v
spodnjem podpoglavju [Struktura](#struktura).

### Pregled

Admin in struktura sta samo povezavi na django admin in vmesnik
[struktura](#struktura).

**Pregled** je prostor s kratkim opisom različnih tabov. Bolj obširna razlaga
sledi.

 - **Kontrole / QR kontrole**: V tabu kontrole dobiš poveyave za vsako kontrolo.
	 Ta povezava dovoli njenemu imetniku pisanje in branje rezultatov (kako je
	 katera ekipa katere kategorije opravljala nalogo na KT-ju). **QR kontrola**
	 vključuje še QR kode teh povezav.

 - **Proge**: prikaže tabelo za vsako progo. Tabela prikazuje čas prvega vnosa
	 rezultatov za par *(ekipa, kontrola)*. Ta tab je namenjen pregledu
	 lokacije/napredovanja ekip.

 - **Rezultati**: Prikazuje rezultate po kategorijah.


### Struktura

Povezava na strukturo te vodi do vmesnika za vnos/spremembo tekmovanja. Tukaj
dodaš specifike svojega tekmovanja.

Sledi opis vsakega elementa, ki opisujejo Tekmovanje.

 - **Tekmovanje**: Drži informacijo o letu in imenu tekmovanja.

 - **Kategorija**: Kategorija, ki ji pripradajo ekipe. Opisana je z *ime*nom in
	 *tekmovanje*m.

 - **Ekipa**: Ekipa pripada kategoriji. Vsaka ekipa mora imeti še *številko*,
	 *rod*, *ime*, *število članov* (privzeto 5) in ali je v konkurenci (privzeto
	 res). Poleg tega ima lahko ekipa določen še *kontakt*, in *id* Sportindent
	 čipa.

 - **Dan**: Dan tekmovanja. Ima določeno *tekmovanje*, (zaporedno) *število* in
	 *datum*. Uporabljen za grupiranje KT-jev.

 - **KT**: Fizična kontrolna točka. Določeno ima *ime* in *vrstni red* (ki se
	 generaira sam iz imena in ni dostopen uporabniku za spreminjanje). Poleg tega
	 ima lahko določeno še *lokacijo*, *kontrolorje*, njihov *kontakt* in *id*-ja
	 do dveh Sportindent čipirnih postaj. KT je vezan samo na dan tekmovanja in ne
	 na kategorijo, ker je lahko KT za različne kategorije.

 - **Proga**: Opisuje progo. Ima *kategorijo*, KT-ja *start* in *cilj*, *časovnico*,
	 *odbitek* in *čas*. Ko ekipa na progi preseže določeno časovnico, na *čas* časa
	 izgubi *odbitek* točk.

 - **Kontrola**: Kontrola za določeno progo in KT. Poleg teh informacij ima še
	 ime in lahko ima časovnico (za individualni umik kontrolnih točk). Združuje
	 **naloge** za isto kategorijo na isti KT.

 - **Naloga**: Določa nalogo na kontrolni točki. Vsaka določi *kontrolo*, *ime*, *min
			točke*, *max točke* in *ponovitve*.

	 - *Kontrola* je **Kontrola**, kjer se naloga izvaja.

	 - *Ime* določa ime naloge (to vidi kontrolor, ko zapisuje uspešnost ekipe).

	 - *Min* in *max točke* določajo najmanjše in največje možno število točk,
		 pridobljenih pri tej nalogi.

	 - *Ponovitve* pomenijo, kolikokrat se naloga izvaja (npr. če imamo nalogo
		 topo testi, lahko damo ponovitve 5 in bo Volčjak sam ponudil okna za vsak
		 topo test posebej).


	Obnašanje nalog se spreminja glede na izbrano vrsto naloge:

	 - **Naloga**: najenostavnejša verzija, edini možnosti sta *min* ali *max*
		 točk.

	 - **NalKT**: predstavlja nalogo KT in ekipi dodeli *max* točk, če ekipa na KT
		 pride.

	 - **NalNum**: določi se še *korak*, ki mora deliti *min točke* - *max točke*
		 in določa, s kakšno natančnostjo kontrolor vnaša dobljene točke.

	 - **NalHitS**, **NalHitC**: Povezana skozi *cilj* na **NalHitS**.
		 Predstavljata start in cilj hitrostne etape in se ju vedno dodaja v paru.

	 - **NalPros**, **NalProC**: Povezana skozi *cilj* na **NalProS*.
		 Predstavljata start in cilj proge in sta avtomatično dodana oz. popravljena
		 pri dodajanju oz. popravljanju **Proge**.

	 - **NalCas**: Predstavlja hitrostno nalogo, pri kateri se čas vpiše.

 - **Rezultat**: predstavlja dosežek *ekipe* pri *nalogi*. Drži še *mrtvi čas*,
	 *čas* (kdaj je ekipa prišla na KT), *čas vnosa* (kdaj je bil rezultat dodan),
	 *zadnji popravek* (kdaj je bil rezultat nazadnje popravljen). Rezultati prav
	 tako obstajajo v različnih oblikah in so vezani na naloge pripadajoče oblike.
	 Definirajo način vnosa za nalogo in izračun točk za dani vnos.

	 - **Rezultat**: Kontrolor s kljukico (v checkbox-u) označi *max*, brez pa
		 *min*.

	 - **RezKT**: Kot Rezultat, vendar je checkbox skrit in vedno aktiviran.

	 - **RezNum**: Namesto kljukice kontrolor direktno vnese število točk.

	 - **RezHitS**, **RezHitC**: Povezana s *cilj* na **RezHitS**. Pri obstoju
		 enega in spremembi/dodajanju drugega se na pripadajoči **NalHitS** kliče
		 *calculate_points*. Ta metoda za vse obstoječe pare **RezHitS**,
		 **RezHitC** izračuna točke (ki so linearno razporejene med časom
		 najhitrejše ekipe in njihovim dvojnim časom).

	 - **RezProS**, **RezProC**: Povezana s *cilj* na **RezProS**. Pri obstoju
		 enega in spremembi/dodajanju drugega se na **RezProS** kliče
		 *calculate_points*. Ta metoda izračuna čas ekipe na progi, odšteje mrtvi
		 čas, izračuna koliko časa je ekipa izven časovnice in primerno odšteje
		 točke. Trenutno se zaokrožuje odbite točke, ne pa prekoračen čas.

	 - **RezCas**: poleg zgoraj naštetega hrani še čas izvajanja. Ob vsaki
		 spremembi, se na pripadajoči **NalCas** kliče *calculate_points*, ki
		 ponovno izračuna točke za vse rezulatate (**RezCas**) te naloge
		 (**NalCas**).



Hranjenje časa prihoda in mrtvega časa v Rezultatih je potratno, saj se hranijo
za vsako nalogo posebej. To je treba še nekako ločiti (in bo ločeno).
