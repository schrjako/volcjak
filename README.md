# Volčjak

## Installation guide

After cloning the repository, change into its root directory and run:
```
$ git checkout beta
```

### Requirements

All packages required are listed in *requirements.txt* and you can install them
with:
```
$ pip3 install -r requirements.txt
```


### Database setup

Volčjak uses a Postgresql database. You need to correct *volcjak/settings.py*
entry of the databases (line ~79).

When you have the databse connection configured, you should
[migrate](https://docs.djangoproject.com/en/4.0/topics/migrations/) all
migrations:

```
$ python manage.py migrate
```

You will also need a superuser for the django admin site:

```
$ python manage.py createsuperuser
```

### Configuration details

As stated in the official django documentation, **DEBUG** should be set to False
in production. That also requires you to set the **ALLOWED_HOSTS** variable
(both in *volcjak/settings.py*).

You should also set **HASHIDS_SALT['base']** to a random value.

### Requirements explanation

Django-qr-code is required for displaying qr-codes.

The package packaging is used for version number parsing.

## Usage guide

After cloning, migrating, and configuring Volčjak, you can run it. Creating a
*Tekmovanje* is done from the django admin interface (which you need your
previously created superuser for). After creating a Tekmovanje, you can access
Volčjak via the two links you get in the *Tekmovanja* tab.

The first link connects you to the general overview of the selected Tekmovanje.
This is the part that is used during the competition. More information can be
found in the subsection [Pregled](#pregled) below.

The second one opens the interface for defining the specifics of the tekmovanje.
This could be done exclusively through the django admin, but that becomes a lot more
complex with multiple Tekmovanjes (because struktura interface limits your
choices for parent entries of the correct Tekmovanje, which also limits the
potential for incorrectly adding a chosen entry). More information can be found
in the subsection [Struktura](#struktura) below.


### Pregled

Admin and struktura are only links to the django admin and the struktura
interface respectively.

**Pregled** is the place with a very bare description for each of the following
tabs. This is described more extensively below.

 - **Kontrole / QR kontrole**: In the tab kontrole you get links to each
	 Kontrola. This link allows whoever has it to read and write results (the
	 points any team of the right category got on that kontrola). **QR kontrola**
	 includes QR codes for these links.

 - **Proge**: Proge displays a table for each proga. This table shows the first
	 time of an entry saved for each *(team, kontrola)* pair. This tab's intended
	 use is an overview of the teams' location/progress.

 - **Rezultati**: Rezultati displays a table of results for each category.


### Struktura

The struktura link will lead you to the main tekmovanje configuring site. There
you can add the specific information for your tekmovanje.

A longer and at the moment up to date description of struktura in slovene can be
found at [README.sl.md#Struktura](README_sl.md#struktura).

Here follows a description of each element type that is used to describe a
Tekmovanje.

 - **Tekmovanje**: Specifies the day and year of the competition.

 - **Kategorija**: A category teams can be a part of.

 - **Ekipa**: A team that competes. The possibilities for the category are all
	 previously added categories.

 - **Dan**: A day of the competition. At least one is needed for a competition.

 - **KT**: A point where teams are graded. This is bound only to a competition
	 day and not to a certain category (it can however be exclusive to a
	 category).

 - **Proga**: Describes a 'track'. This is used to connect KTs with kategorijas
	 and calculate points for time used to complete the 'track'.

 - **Kontrola**: A task that is specific for a proga and KT. Groups 'Naloga'
	 elements (see next list item).

 - **Naloga**: A task with a range of possible point amounts. The general
	 'Naloga' type only allows max or min points, whereas the 'NalNum' type allows
	 an entry in between max and min points with the precision of 'korak'.
